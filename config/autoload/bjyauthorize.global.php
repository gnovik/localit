<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/8/13
 * Time: 3:25 PM
 */

use \Localit\Entity\TaskAbstract as Task;

return [
    'bjyauthorize' => array(
        // Using the authentication identity provider, which basically reads the roles from the auth service's identity
        'identity_provider' => 'BjyAuthorize\Provider\Identity\AuthenticationIdentityProvider',

        'role_providers'        => array(
            // using an object repository (entity repository) to load all roles into our ACL
            'BjyAuthorize\Provider\Role\ObjectRepositoryProvider' => array(
                'object_manager'    => 'doctrine.documentmanager.odm_default',
                'role_entity_class' => 'Application\Entity\Role',
            ),
        ),
        'resource_providers' => array(
            'BjyAuthorize\Provider\Resource\Config' => array(
                'pants' => [],
                'task' => [],
                'set-task-status' => []
            ),
        ),
        'rule_providers' => array(
            'BjyAuthorize\Provider\Rule\Config' => array(
                'allow' => [
                    // allow guests and users (and admins, through inheritance)
                    // the "wear" privilege on the resource "pants"
                    [['user'], 'task', 'view'],

                    [['admin', 'manager'], 'task', ['edit', 'create']],
                    [['admin', 'translator'], 'task', ['enter_estimate', 'change_status']],

                    [['admin', 'translator'], 'set-task-status', [
                        Task::STATUS_OPEN        . '_' . Task::STATUS_IN_PROGRESS,
                        Task::STATUS_IN_PROGRESS . '_' . Task::STATUS_DONE,
                        Task::STATUS_DONE        . '_' . Task::STATUS_REOPENED,
                        Task::STATUS_DONE        . '_' . Task::STATUS_IN_PROGRESS,
                        Task::STATUS_REOPENED    . '_' . Task::STATUS_DONE,
                        Task::STATUS_REOPENED    . '_' . Task::STATUS_IN_PROGRESS

                    ]],

                    [['admin', 'manager'], 'set-task-status', [
                        Task::STATUS_DONE     . '_' . Task::STATUS_REOPENED,
                        Task::STATUS_DONE     . '_' . Task::STATUS_APPROVED,
                        Task::STATUS_APPROVED . '_' . Task::STATUS_CLOSED
                    ]],
                ],

                // Don't mix allow/deny rules if you are using role inheritance.
                // There are some weird bugs.
                'deny' => [
                    // ...
                ],
            ),
        ),
        'guards' => [
            'BjyAuthorize\Guard\Controller' => [
                ['controller' => 'Application\Controller\Index', 'action' => 'index', 'roles' => ['guest', 'user']],

                ['controller' => 'Application\Controller\CreateTask', 'action' => 'index', 'roles' => ['manager']],
                ['controller' => 'Application\Controller\Task',   'action' => 'edit', 'roles' => ['manager']],
                ['controller' => 'Application\Controller\Task',   'action' => 'translate', 'roles' => ['admin', 'translator']],

                ['controller' => 'Application\Controller\Backend', 'action' => ['index', 'languages'], 'roles' => ['manager']],

                ['controller' => 'Application\Controller\Report', 'action' => ['index'], 'roles' => ['manager', 'translator']],

                ['controller' => 'Localit\Controller\Create',   'roles' => ['manager']],
                ['controller' => 'Localit\Controller\Edit',     'roles' => ['manager']],
                ['controller' => 'Localit\Controller\Edit',    'action' => ['comment'], 'roles' => ['user']],

                ['controller' => 'Localit\Controller\ReferenceBook',     'roles' => ['manager']],

                ['controller' => 'Localit\Controller\Translate', 'action' => ['upload-translation', 'approve-translation'], 'roles' => ['admin', 'translator']],
                ['controller' => 'Localit\Controller\Translate', 'action' => ['download-translation'], 'roles' => ['user']],

                ['controller' => 'Localit\Controller\TaskPatch', 'action' => [null], 'roles' => ['manager', 'translator']],
                ['controller' => 'Localit\Controller\TaskPatch', 'action' => ['status'], 'roles' => ['manager', 'translator']],
                ['controller' => 'Localit\Controller\TaskPatch', 'action' => ['estimated-date'], 'roles' => ['admin', 'translator']],
                ['controller' => 'Localit\Controller\Help', 'action' => ['explain'], 'roles' => ['guest']],
                ['controller' => 'Localit\Controller\Report', 'action' => ['index'], 'roles' => ['user']],


                ['controller' => 'zfcuser', 'action' => 'register', 'roles' => 'guest'],
                ['controller' => 'zfcuser', 'action' => ['index', 'login', 'authenticate'], 'roles' => 'guest'],
                ['controller' => 'zfcuser', 'action' => ['logout', 'changepassword', 'changeemail'], 'roles' => 'user'],


            ]
        ]
    )
];