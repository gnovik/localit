<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 12/3/13
 * Time: 3:03 PM
 */


return [
    'service_manager' => [
        'aliases' => array(
        ),
        'factories' => [
            'Localit\UserRepository' => function($sm)   {
                return $sm->get('UserRepository');
            }
        ]
    ]
];