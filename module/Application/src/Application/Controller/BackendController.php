<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/21/13
 * Time: 5:16 PM
 *
 * Controller used to handle localit backend
 *
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Localit\Controller\Traits\FetchDocumentManagerTrait;
use Zend\Debug\Debug;
use Zend\Stdlib\Hydrator;

class BackendController extends AbstractActionController {

    use FetchDocumentManagerTrait;

    /**
     * @return array|ViewModel
     *
     * Render backend index page
     */
    public function indexAction()   {
        $view = new ViewModel();

        $view->addChild($this->languagesAction(), 'languages');

        return $view;
    }

    /**
     * @return ViewModel
     *
     * Handle languages - rates save request
     *
     */
    public function languagesAction()   {
        $view = new ViewModel();

        $rep = $this->getDM()->getRepository('\Localit\Entity\Language');

        $request = $this->getRequest();
        if ($request->isPost()) {


            foreach ($request->getPost() as $id => $data)   {
                Debug::dump($data, $id);
                $entity = $rep->find($id);

                $hydrator = new Hydrator\ClassMethods();
                $hydrator->hydrate($data, $entity);

                $this->getDM()->persist($entity);
                $this->getDM()->flush();
            }
        }



        $languages = $rep->findAll();

        $view->setVariable('languages', $languages);
        $view->setTemplate('application/backend/languages');

        return $view;
    }

} 