<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/1/13
 * Time: 12:18 PM
 *
 * Controller handle task creation life-cycle programmed by Localit module
 *
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class CreateTaskController extends AbstractActionController {

    /**
     * Handle task creation master
     *
     * @return array|ViewModel
     * @throws \Exception
     *
     */
    public function indexAction()    {
        $step = $this->params('step');

        $viewModel = new ViewModel(array());
        $viewModelForm = new ViewModel(array());
        $viewModel->addChild($viewModelForm, 'form');


        $errorMessage = $this->params('errorMessage', false);
        $viewModelForm->setVariable('errorMessage', $errorMessage);

        switch ($step)  {
            case 1:
                $viewModelForm->setTemplate('application/create-task/index-1');

                break;

            case 2:
                $task = $this->params('task'); //can be object (when request forwarded after file upload)

                $viewModelForm->setVariable('task', $task);
                $viewModelForm->setTemplate('application/create-task/index-2');
                break;

            case 3:
                $task = $this->params('task'); //can be object (when request forwarded after file upload)

                $viewModelForm->setVariable('task', $task);
                $viewModelForm->setTemplate('application/create-task/index-3');
                break;


            default;
                throw new \Exception("Step `$step` isn't allowed");

        }

        return $viewModel;
    }

} 