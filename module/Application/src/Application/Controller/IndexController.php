<?php
/**
 * Controller render Localit Dashboard
 *
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Debug\Debug;
use Localit\Controller\Traits\FetchDocumentManagerTrait;
use Localit\Entity;


class IndexController extends AbstractActionController
{
    use FetchDocumentManagerTrait;

    public $currentManagerId;

    public function indexAction()
    {
        $user = $this->zfcUserAuthentication()->getIdentity();
        if (!$user) {
            return $this->redirect()->toRoute('zfcuser');
        }

        $this->currentManagerId = $this->params()->fromQuery('manager');
        if (!$this->currentManagerId)   {
            if ($user->isManager()) {
                $this->currentManagerId = (string)$user->getId();
            }
        }

        $view = new ViewModel();

        if ($action = $this->params()->fromQuery('action'))    {

            $task = $this->params()->fromQuery('task', false);

            if ($task)  {

                $documentSet = $this->getDM()->createQueryBuilder('\Localit\Entity\TaskAbstract')
                    ->update()
                    ->multiple(true)
                    ->field('instanceNumber')
                    ->in($task);

                switch($action) {
                    case 'approve':
                        $result = $documentSet
                            ->field('status')->set(Entity\TaskAbstract::STATUS_APPROVED)
                            ->field('approved_date')->set(new \DateTime())
                            ->getQuery()->execute();

                        break;

                    case 'close':
                        $result = $documentSet->field('status')->set(Entity\TaskAbstract::STATUS_CLOSED)
                            ->getQuery()->execute();

                        break;

                    case 'archive':
                        $result = $documentSet->field('archived')->set(true)
                            ->getQuery()->execute();

                        break;

                    case 'trash':
                    case 'delete':
                        $result = $documentSet->field('trash')->set(true)
                            ->getQuery()->execute();

                        break;

                    default:
                        $result = false;
                        //wron action, ignore

                }

                if ($result && is_array($result))    {
                    if ($result['ok'])  {
                        if ($result['updatedExisting']) {
                            $view->setVariable('updatedNumber', $result['n']);
                        }
                        else    {
                            $view->setVariable('updatedNumber', 0);
                        }
                        $view->setVariable('actionSuccess', true);
                    }
                    else    {
                        $view->setVariable('actionSuccess', false);
                        $view->setVariable('error', $result['err']);
                    }
                }
            }

        }


        $taskList = new ViewModel();
        $taskList->setTemplate('application/index/task-list');


        $sorter = $this->taskSorter();

        $filter = $this->taskFilter();
        $filter->setRepository($this->getDM()->getRepository('Localit\Entity\TaskAbstract'));

        $taskSet = $filter->getDocumentSet()->sort($sorter->getCondition());


        $totalSum = 0;
        foreach ($taskSet as $task) {
            $totalSum += $task->getTotalSum();
        }

        $taskList->setVariable('taskSet', $taskSet);
        $taskList->setVariable('totalSum', round($totalSum));
        $taskList->setVariable('allowedStatuses', Entity\TaskAbstract::$allowedStatuses);

        $view->setVariable('activeFilter', $filter->getCalculatedParam());
        $view->setVariable('activeSort', $sorter->getCalculatedParam());
        $view->setVariable('sortDirection', $sorter->sortDirection);
        $view->setVariable('filterSet', $filter->getOptionSet());
        $view->setVariable('sortSet', $sorter->getOptionSet());




        $view->addChild($taskList, 'taskList');


        $managers = $this->getDM()->getRepository('Application\Entity\User')->getUsersByRole('manager');
        $view->setVariable('managers', $managers);
        $view->setVariable('manager', $this->currentManagerId);


        switch (true)   {
            case $user->isManager():
            case $user->isAdmin():
                //show manager dashboard
                $taskList->setVariable('taskEnterRoute', 'application/edit-task');

                $view->setTemplate('application/dashboard/manager');

               break;

            case $user->isTranslator():
                //show translator dashboard
                $taskList->setVariable('taskEnterRoute', 'application/translate-task');
                $view->setTemplate('application/dashboard/translator');

                break;

            default:
                return $this->redirect()->toRoute('zfcuser');
        }


        return $view;
    }

    public function opDispatchAction()    {

    }
}
