<?php
/**
 * Show report
 *
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/21/13
 * Time: 5:16 PM
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Localit\Controller\Traits\FetchDocumentManagerTrait;
use Zend\Debug\Debug;
use Zend\Stdlib\Hydrator;
use Application\Form;

class ReportController extends AbstractActionController {

    use FetchDocumentManagerTrait;

    public function indexAction()   {
        $view = new ViewModel();

        $rep = $this->getServiceLocator()->get('TaskRepository');

        $form = new Form\ReportFilterForm();
        $view->setVariable('form', $form);

        $conditions = [
            'published' => true,
            'archived'  => ['$ne' => 'true'],
            'trash'  => ['$ne' => 'true']
        ];


        $taskSet = $rep->findBy($conditions);

        $view->setVariable('taskSet', $taskSet);

        return $view;
    }


} 