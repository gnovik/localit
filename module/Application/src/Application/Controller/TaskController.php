<?php
/**
 *
 * Provide task functionality after creation
 *
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/11/13
 * Time: 10:31 AM
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Localit\Controller\Traits\FetchDocumentManagerTrait;

class TaskController extends AbstractActionController {

    use FetchDocumentManagerTrait;

    public function editAction()    {

        $user = $this->zfcUserAuthentication()->getIdentity();
        if (!$user) {
            return $this->redirect()->toRoute('zfcuser');
        }

        $view = new ViewModel();

        $task = $this->params('task');
        if (is_scalar($task))   {
            $rep = $this->getServiceLocator()->get('TaskRepository');
            $task = $rep->getByNumber($task);
        }

        if (!$task) {
            throw new \Exception('Task is missing');
        }

        $view->setVariable('task', $task);


        return $view;
    }

    public function translateAction()    {

        $user = $this->zfcUserAuthentication()->getIdentity();
        if (!$user) {
            return $this->redirect()->toRoute('zfcuser');
        }

        $view = new ViewModel();

        $task = $this->params('task');
        if (is_scalar($task))   {
            $rep = $this->getDM()->getRepository('Localit\Entity\TaskAbstract');
            $task = $rep->getByNumber($task);
        }

        if (!$task) {
            throw new \Exception('Task is missing');
        }

        $view->setVariable('task', $task);
        $view->setVariable('messages', $this->params('messages'));


        return $view;
    }

} 