<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/8/13
 * Time: 12:42 PM
 */

namespace Application\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use BjyAuthorize\Acl;

/** @ODM\Document(db="localit", collection="roles") */
class Role implements Acl\HierarchicalRoleInterface {

    /** @ODM\Id */
    protected $id;

    /** @ODM\Field */
    protected $role_id;

    /**
     * @ODM\EmbedOne(
     *     strategy="set",
     *     targetDocument="Role"
     * )
     */
    protected $parent;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $role_id
     */
    public function setRoleId($role_id)
    {
        $this->role_id = $role_id;
    }

    /**
     * @return mixed
     */
    public function getRoleId()
    {
        return $this->role_id;
    }




} 