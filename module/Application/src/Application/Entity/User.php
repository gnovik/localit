<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/8/13
 * Time: 10:48 AM
 */

namespace Application\Entity;

use ZfcUser\Entity\UserInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use BjyAuthorize\Provider\Role\ProviderInterface as ProvideRoleInterface;
use Doctrine\Common\Collections\ArrayCollection;

/** @ODM\Document(db="localit", collection="profile", repositoryClass="\Application\Repository\User") */
class User implements UserInterface, ProvideRoleInterface {


    /** @ODM\Id */
    protected $id;

    /** @ODM\Field */
    protected $user_name;

    /** @ODM\Field */
    protected $email;

    /** @ODM\Field */
    protected $display_name;

    /** @ODM\Field */
    protected $password;

    /** @ODM\Field */
    protected $state;

    /** @ODM\EmbedMany(targetDocument="Application\Entity\Role") */
    protected $roles;


    public function __construct()   {
        $this->roles = new ArrayCollection();
    }







    /**
     * @param mixed $display_name
     */
    public function setDisplayName($display_name)
    {
        $this->display_name = $display_name;
    }

    /**
     * @return mixed
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $user_name
     */
    public function setUserName($user_name)
    {
        $this->user_name = $user_name;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles->getValues();
    }

    /**
     * Add a role to the user.
     *
     * @param Role $role
     *
     * @return void
     */
    public function addRole($role)
    {
        $this->roles[] = $role;
    }

    /**
     * Check if user has specific role
     *
     * @param $roleId
     *
     * @return bool
     */
    public function hasRole($roleId)    {
        foreach ($this->roles as $role)  {
            if ($role->getRoleId() == $roleId)  {
                return true;
            }
        }

        return false;
    }

    public function getRoleIds()    {
        $roles = [];
        foreach ($this->roles as $role)  {
            $roles[] = $role->getRoleId();
        }

        return $roles;
    }

    public function isManager() {
        return $this->hasRole('manager');
    }

    public function isTranslator() {
        return $this->hasRole('translator');
    }

    public function isAdmin() {
        return $this->hasRole('admin');
    }



} 