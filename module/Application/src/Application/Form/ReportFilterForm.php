<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/22/13
 * Time: 10:16 AM
 */

namespace Application\Form;

use Zend\Form;

class ReportFilterForm extends Form\Form    {

    public function __construct()   {
        parent::__construct('report_filter');
        $this->add([
            'name' => 'from',
            'options' => [
                'label' => 'From'
            ],
            'attributes' => [
                'value' => date('Y-m-d', strtotime('first day of this month')),
                'class' => 'form-control',
            ]
        ]);

        $this->add([
            'name' => 'to',
            'options' => [
                'label' => 'To'
            ],
            'attributes' => [
                'value' => date('Y-m-d', strtotime('last day of this month')),
                'class' => 'form-control',
            ]
        ]);
    }
} 