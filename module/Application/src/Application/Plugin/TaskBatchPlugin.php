<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/26/13
 * Time: 9:43 AM
 */

namespace Application\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Localit\Entity;

class TaskBatchPlugin extends AbstractPlugin  {

    protected $paramName = '';
    public $isActive = false;

    public function __invoke()  {

        return $this;
    }

    public function getCalculatedParam()   {
        $ctrlParams = $this->getController()->params();
        $activeState = $ctrlParams->fromQuery($this->paramName, false);

        if (!$activeState) {
            $this->isActive = false;
            return $ctrlParams->fromQuery('persist_' . $this->paramName, false);
        }
        else    {
            $this->isActive = true;
            return $activeState;
        }
    }


} 