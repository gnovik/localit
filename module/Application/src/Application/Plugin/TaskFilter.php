<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/26/13
 * Time: 9:43 AM
 */

namespace Application\Plugin;


use Localit\Controller\Traits\FetchDocumentManagerTrait;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Localit\Entity;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class TaskFilter extends TaskBatchPlugin implements ServiceLocatorAwareInterface {
    use ServiceLocatorAwareTrait;
    use FetchDocumentManagerTrait;

    protected $baseCondition = [
        'published' => true,
        'trash'     => ['$ne' => true],
        'archived'  => ['$ne' => true]
    ];

    protected $paramName = 'filter';

    protected $repository;

    public function getRepository() {
        if ($this->repository)  {
            return $this->repository;
        }
        else    {
            throw new \Exception('Repository was not set');
        }
    }

    public function setRepository($repository)  {
        $this->repository = $repository;
    }

    public function getDocumentSet()    {
        return $this->getRepository()->findBy($this->getCondition());
    }

    public function getCalculatedParam()   {
        $ctrlParams = $this->getController()->params();
        $activeState = $ctrlParams->fromQuery($this->paramName, false);
        $persistState = $ctrlParams->fromQuery('persist_' . $this->paramName, false);

        return $activeState === false ? $persistState : $activeState;
    }

    public function getCondition($param = null)  {
        $condition = $this->baseCondition;
        if (is_null($param))    {
            $param = $this->getCalculatedParam();
        }

        switch($param)  {
            case 'all':
            case false:
                break;

            case 'trash':
                $condition['trash'] = true;
                unset($condition['archived']);
                break;

            case 'archived':
                $condition['archived'] = true;
                break;

            default:
                if (!array_key_exists($param, Entity\TaskAbstract::$allowedStatuses))    {
                    break; //wrong filter
                }

                $condition['status'] = $param; //filter by status
                break;
        }

        $userId = $this->getController()->currentManagerId;
        if ($userId && $userId !== 'all')   {
            $condition['owner.$id'] = new \MongoId($userId);
        }

        return $condition;
    }

    public function getOptionSet()  {
        $optionSet = [
            'all'                           => 'All',
            Entity\TaskAbstract::STATUS_OPEN        => 'Created',
            Entity\TaskAbstract::STATUS_IN_PROGRESS => 'Started',
            Entity\TaskAbstract::STATUS_DONE        => 'Done',
            Entity\TaskAbstract::STATUS_APPROVED    => 'Approved',
            Entity\TaskAbstract::STATUS_CLOSED      => 'Closed',
            'archived'                      => 'Archieved',
            'trash'                         => 'Deleted',
        ];

        foreach ($optionSet as $option => &$title)  {
            $condition = $this->getCondition($option);
            $title = [
                'count' => $this->repository->findBy($condition)->count(),
                'title' => $title
            ];
        }

        return $optionSet;
    }


} 