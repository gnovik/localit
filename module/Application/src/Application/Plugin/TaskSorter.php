<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/26/13
 * Time: 9:43 AM
 */

namespace Application\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Localit\Entity;

class TaskSorter extends TaskBatchPlugin  {

    protected $baseCondition = [
    ];

    protected $paramName = 'sort';

    public $sortDirection = '';


    public function getCondition()  {
        $condition = $this->baseCondition;
        $param = $this->getCalculatedParam();

        $direction = $this->getController()->params()->fromQuery('sort_direction');

        switch($param)  {
            case '':
            case false:
                break;

            case 'estimatedDate':
            case 'startDate':
            case 'creationDate':
            case 'approvedDate':
                if ($direction === false) {
                    $direction = 'ASC';
                }
                elseif ($this->isActive)    {
                    $direction = $direction === 'ASC' ? 'DESC' : 'ASC';
                }
                $condition[$param] = $direction;
                break;
        }

        $this->sortDirection = $direction;


        return $condition;
    }

    public function getOptionSet()  {
        return [
            'creationDate' => 'Date'
        ];
    }

} 