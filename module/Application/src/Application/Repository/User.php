<?php
namespace Application\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ODM\MongoDB\Cursor;
use Doctrine\ODM\MongoDB\LockMode;

class User extends DocumentRepository   {


    public function getUsersByRole($role)   {
        return $this->uow->getDocumentPersister('\Application\Entity\User')->loadAll([
            'roles.role_id' => $role
        ]);
    }


    public function fetchUsersEmails(Cursor $cursor) {
        $list = [];
        foreach ($cursor as $doc)   {
            $list[] = $doc->getEmail();
        }

        return $list;
    }


} 