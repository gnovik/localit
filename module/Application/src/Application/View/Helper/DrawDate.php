<?php

namespace Application\View\Helper;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 10/31/13
 * Time: 12:07 PM
 */

use Zend\View\Helper\AbstractHelper;

class DrawDate extends AbstractHelper {

    const DATE_AND_TIME = 'datetime';
    const DATE_ONLY = 'date';
    const TIME_ONLY = 'time';

    public function __invoke($entity, $method, $label = '', $addTime = self::DATE_AND_TIME)  {
        $data = $entity->$method();
        if ($data instanceof \DateTime) {
            $timestamp = $data->getTimestamp();
        }
        else    {
            $timestamp = '';
        }

        return $this->getView()->render('application/helpers/draw-date', [
            'label' => strlen($label) > 0 ? $label . ':' : '',
            'timestamp' => $timestamp,
            'format' => $addTime
        ]);
    }
} 