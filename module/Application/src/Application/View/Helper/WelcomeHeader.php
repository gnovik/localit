<?php

namespace Application\View\Helper;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 10/31/13
 * Time: 12:07 PM
 */

use Zend\View\Helper\AbstractHelper;

class WelcomeHeader extends AbstractHelper {

    public function __invoke()  {
        return $this->getView()->render('application/index/welcome-header');
    }
} 