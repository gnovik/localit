<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Localit;

use MyProject\Proxies\__CG__\stdClass;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use \Zend\View\HelperPluginManager;
use Zend\View as ZView;
use \Zend\EventManager\EventManager;
use \Localit\Notification\NotificationManager;

use Zend\Mail\Message;
use Zend\Mail\Transport;
use Zend\Di\Di;
use Zend\Di\Config as DiConfig;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ServiceProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $app = $e->getApplication();
        $eventManager = $app->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach('render', array($this, 'onRender'), 100);

        $sm = $app->getServiceManager();

        $eventManager = $sm->get('localitEventManager');
        $eventManager->attachAggregate($sm->get('localitNotificationManager'));

//        $eventManager->attach('create_task', function() {
//            var_dump('CREATE TASK');
//        }, 100);


        $serviceManager = $app->getServiceManager();
        $doctrineObjectManager = $serviceManager->get('doctrine.documentmanager.odm_default');

        $localitEventListener = $serviceManager->get('localitEventListener');
        $doctrineObjectManager->getEventManager()->addEventListener(['prePersist', 'preUpdate'], $localitEventListener);

    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }


    public function getServiceConfig()
    {
        return array(
            'alias' => [
                '\Localit\Entity\User' => '\Application\Entity\User'
            ],
            'factories' => array(
                'localitNotificationManager' => function () {
                        return new NotificationManager();
                    },
                'Localit\MailNotificationDI' => function ($sm) {

                        $config = $sm->get('Config');
                        $diConfig = $config['mail_di'];

                        $di = new Di();
                        $di->configure(new DiConfig($diConfig));

                        return $di;
                    },
                'localitEventManager' => function () {
                        return new EventManager('localit');
                    },
                'localitEventListener' => function ($sm) {
                        $eventListener = new \Localit\Listener\DoctrineEvents();

                        return $eventListener;
                    },
                'localitUploadTaskFileForm' => function ($sm) {
                        $form = new Form\UploadTaskFile();

                        return $form;
                    },
                'localitUploadTranslationForm' => function ($sm) {
                        $form = new Form\UploadTranslation();

                        return $form;
                    },
                'localitCreateTaskForm' => function ($sm) {

                        $doctrineObjectManager = $sm->get('doctrine.documentmanager.odm_default');

                        $form = new Form\CreateTask($doctrineObjectManager);

                        return $form;
                    },
                'localiteEditTaskContentForm' => function ($sm) {
                        $form = new Form\TranslationForm\EditTaskContentForm();

                        return $form;
                    },
                'localiteApproveTranslationContentForm' => function ($sm) {
                        $form = new Form\TranslationForm\ApproveTranslationContentForm();

                        return $form;
                    },
                'localiteTranslationForm' => function ($sm) {
                        $form = new Form\TranslationForm();

                        return $form;
                    },
                'TaskRepository' => function ($sm) {
                        $dm = $sm->get('doctrine.documentmanager.odm_default');
                        return $dm->getRepository('Localit\Entity\TaskAbstract');
                    },
                'Localit\UserRepository' => function () {
                        $b = new \stdClass();
                        $b->b = 2;
                        return $b;
                    },
                'Localit\JsonRenderer' => function ($services) {

                        $renderer = new ZView\Renderer\JsonRenderer();

                        return $renderer;
                    },
                'Localit\RestfulJsonStrategy' => function ($services) {
                        $renderer = $services->get('Localit\JsonRenderer');
                        return new ZView\Strategy\JsonStrategy($renderer);
                    },


                'LocalitConfig' => function ($services) {
                        $config = $services->get('Config');

                        return array_key_exists('localit', $config) ? $config['localit'] : [];
                    },
                'LocalitRoutesConfig' => function ($services) {
                        $config = $services->get('LocalitConfig');

                        return array_key_exists('routes', $config) ? $config['routes'] : [];
                    }
            )
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'localitShowAddTaskForm' => function (HelperPluginManager $helperPluginManager) {

                        $viewHelper = new View\Helper\ShowAddForm();

                        return $viewHelper;
                    },
                'localitShowEditTaskForm' => function (HelperPluginManager $helperPluginManager) {

                        $viewHelper = new View\Helper\ShowEditForm();

                        return $viewHelper;
                    },
                'localitShowTranslateTaskForm' => function (HelperPluginManager $helperPluginManager) {

                        $viewHelper = new View\Helper\ShowTranslateForm();

                        return $viewHelper;
                    },
                'localitShowTaskContent' => function (HelperPluginManager $helperPluginManager) {
                        $doctrineObjectManager = $helperPluginManager->getServiceLocator()->get('doctrine.documentmanager.odm_default');

                        $viewHelper = new View\Helper\ShowTaskContent();
                        $viewHelper->setObjectManager($doctrineObjectManager);

                        return $viewHelper;
                    },
                'bootstrapElement' => function () {
                        $viewHelper = new View\Helper\BootstrapElement();

                        return $viewHelper;
                    },
                'renderLog' => function () {
                        $viewHelper = new View\Helper\LogRenderer();

                        return $viewHelper;
                    },
                'taskFileDescription' => function () {
                        $viewHelper = new View\Helper\TaskFileDescription();

                        return $viewHelper;
                    },
                'showWordsCount' => function () {
                        $viewHelper = new View\Helper\ShowWordsCount();

                        return $viewHelper;
                    }
            )
        );
    }


    public function onRender($e)
    {

        $result = $e->getResult();
        if (!$result instanceof ZView\Model\JsonModel) {
            return;
        }

        $app = $e->getTarget();
        $services = $app->getServiceManager();
        $view = $services->get('View');

        $restfulJsonStrategy = $services->get('Localit\RestfulJsonStrategy');
        $events = $view->getEventManager();

        $events->attach($restfulJsonStrategy, 200);


    }

}