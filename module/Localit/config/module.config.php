<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'api' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/localit/api',
                    'defaults' => [
                        '__NAMESPACE__' => 'Localit\Controller',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'reference-book' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/reference/:book[/:id]',
                            'defaults' => [
                                'controller' => 'reference-book'
                            ],
                            'constraints' => [
                                'book' => '[a-z0-9]{1,60}',
                                'id' => '[a-z0-9]{1,24}', //not only mongoid

                            ],

                        ],
                    ],
                    'task-patch' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/task-patch/task/:task',
                            'defaults' => [
                                'controller' => 'task-patch'
                            ],
                        ]
                    ],
                ]

            ],
            'localit' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/localit',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Localit\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                    'create-task-master' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create-task[/:action][/task/:task]',
                            'defaults' => array(
                                'controller' => 'create',
                                'action'     => 'upload-task-file'
                            ),
                        ),
                    ),
                    'edit-task' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/edit-task[/:action][/task/:task]',
                            'defaults' => [
                                'controller' => 'edit',
                                'action'     => 'save-task-meta-info'
                            ],
                        ],
                    ],
                    'comment-task' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/comment-task/task/:task',
                            'defaults' => [
                                'controller' => 'edit',
                                'action'     => 'comment'
                            ],
                        ],
                    ],
                    'upload-translation' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/upload-translation/task/:task',
                            'defaults' => [
                                'controller' => 'translate',
                                'action'     => 'upload-translation'
                            ],
                        ]
                    ],
                    'approve-translation' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/approve-translation/task/:task',
                            'defaults' => [
                                'controller' => 'translate',
                                'action'     => 'approve-translation'
                            ],
                        ]
                    ],
                    'explain-task-status' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/task-status/explain/:status',
                            'defaults' => [
                                'controller' => 'help',
                                'action'     => 'explain'
                            ],
                        ]
                    ],
                    'download-translation' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/task-translation/download/:task/:language',
                            'defaults' => [
                                'controller' => 'translate',
                                'action'     => 'download-translation',
                                'language'   => 'origin'
                            ],
                        ]
                    ],
                    'export-report' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/export-report/[:idList][/:language]',
                            'defaults' => [
                                'controller' => 'report',
                                'action'     => 'index'
                            ],
                        ]
                    ]

                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'localit/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'localit/index/index'     => __DIR__ . '/../view/localit/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Localit\Controller\Create'          => 'Localit\Controller\CreateController',
            'Localit\Controller\Edit'            => 'Localit\Controller\EditController',
            'Localit\Controller\ReferenceBook'   => 'Localit\Controller\ReferenceBookController',
            'Localit\Controller\Translate'       => 'Localit\Controller\TranslateController',
            'Localit\Controller\TaskPatch'       => 'Localit\Controller\TaskPatchController',
            'Localit\Controller\Help'            => 'Localit\Controller\HelpController',
            'Localit\Controller\Report'            => 'Localit\Controller\ReportController',
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'fileOutput' => 'Localit\Plugin\FileOutput',
        )
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __DIR__ . '/../public',
            ),
        ),
    ),
    'localit' => [
        'routes' => [
            'dashboard' => 'dashboard',
            'translate-task' => 'application/translate-task'
        ],
        'create_task_master' => [
            'upload-task-file' => [
                'view' => 'localit/create-task-master/upload-task-file',
                'next' => [
                    'forwardType' => 'redirect',
                    'route' => 'localit/create-task-master',
                    'routeParams' => [
                        'action' => 'approve-export-result'
                    ]
                ]
            ],
            'approve-export-result' => [
                'view' => 'localit/create-task-master/approve-export-result',
                'next' => [
                    'forwardType' => 'redirect',
                    'route' => 'localit/create-task-master',
                    'routeParams' => [
                        'action' => 'save-task-meta-info'
                    ]
                ]
            ],
            'save-task-meta-info' => [
                'view' => 'localit/create-task-master/save-task-meta-info',
                'next' => [
                    'forwardType' => 'redirect',
                    'route' => 'dashboard'
                ]
            ]
        ]
    ],
    'mail_di' => [
        'instance' => array (
            'Zend\View\Resolver\TemplatePathStack' => array (
                'parameters' => array (
                    'paths' => array (
                        'MailTemplate' => __DIR__. '/../view/',
                    ),
                ),
            ),
            'Zend\View\Renderer\PhpRenderer' => array (
                'parameters' => array (
                    'resolver' => 'Zend\View\Resolver\TemplatePathStack',
                ),
            ),
            'Zend\View\Model\ViewModel' => array (
                'parameters' => array (
                    'template' => 'localit/notification/template',
                ),
            ),
            'Zend\Mail\Transport\Sendmail' => array (
                'injections' => array (
                )
            ),
            'Zend\Mail\Message' => array (
                'parameters' => array (
                    'Headers' => 'Zend\Mail\Headers',
                )
            ),
        )
    ]
);
