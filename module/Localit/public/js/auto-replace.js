/**
 * Created by fliak on 11/19/13.
 */


jQuery.prototype.showInline = function()   {
    this.css('display', 'inline');
}

jQuery.prototype.autoReplace = function(config)  {

    config = $.extend({
        api: function() {
            this.success();
        },
        haystack: false,
        startValue: undefined,
        template: 'js/auto-replace-select.ejs',
        propName: undefined

    }, config || {});

    if (config.startValue === undefined && config.haystack)    {
        for (var i in config.haystack)  {
            config.startValue = i;
            break;
        }
    }
    new EJS({url: config.template}).update(this[0], config);
    this.showInline();


    var controlWrapper = this.find('.localit-autoreplace-control-wrapper');
    var control        = controlWrapper.find('.localit-autoreplace-control');
    var titleWrapper   = this.find('.localit-autoreplace-title');
    var errorWrapper   = this.find('.localit-autoreplace-error');
    var loadingWrapper = this.find('.localit-autoreplace-loading');
    var okWrapper      = this.find('.localit-autoreplace-ok');

    var oldValue = config.startValue;

    var setValue = function(newValue, success)    {
        loadingWrapper.css('display', 'inline-block');
        errorWrapper.html('');

        config.api.call({
            success: function() {
                console.log('success2');
                controlWrapper.hide();
                titleWrapper.html(config.haystack ? config.haystack[newValue] : newValue);
                titleWrapper.showInline();
                loadingWrapper.hide();

                okWrapper.css('display', 'inline-block');


                if ($.isFunction(success))  {
                    success(newValue);
                }
            },
            fail: function(message) {
                errorWrapper.html(message || 'Cannot save value');
                loadingWrapper.hide();
            }
        }, newValue, config.propName);
    };

    control.change(function() {
        var currentValue = control.val();

        if (oldValue === currentValue)  {
            return;
        }

        setValue(currentValue);

    });

    titleWrapper.click(function() {
        titleWrapper.hide();
        controlWrapper.showInline();
        oldValue = control.val();
        errorWrapper.html('');
        okWrapper.hide();
    });

    return {
        setValue: setValue
    };
};