$(function()    {
    $('.localit-readmore').each(function()  {
        var readmoreEl = this;

        var text = $(readmoreEl).text();

        if ($.trim(text).length < 75) {
            return;
        }

        var parentEl = $(readmoreEl).parents('.localit-readmore-parent');
        if (parentEl.length === 0)  {
            parentEl = $(readmoreEl).parent();
        }

        $(readmoreEl).addClass('localit-readmore-expandable');
        $(readmoreEl).click(function()  {
            if ($(parentEl).hasClass('localit-readmore-expand'))    {
                $(parentEl).removeClass('localit-readmore-expand');
            }
            else    {
                $(parentEl).addClass('localit-readmore-expand');
            }

        })

//        var a1 = $('<a class="localit-readmore-readmore-link" href="#">...</a>');
//        var a2 = $('<a class="localit-readmore-readless-link" href="#">...</a>');
//        a2.hide();
//
//        $(readmoreEl).after(a1);
//        $(readmoreEl).after(a2);

//        a1.click(function()  {
//            $(parentEl).addClass('localit-readmore-expand');
//            $(parentEl).find('.localit-readmore-readmore-link').hide();
//            $(parentEl).find('.localit-readmore-readless-link').show();
//
//            return false;
//        });
//
//        a2.click(function()  {
//            $(parentEl).removeClass('localit-readmore-expand');
//            $(parentEl).find('.localit-readmore-readless-link').hide();
//            $(parentEl).find('.localit-readmore-readmore-link').show();
//
//
//            return false;
//        });

    });

    $('.localit-timestamp').each(function() {
        var timestamp = $.trim($(this).text());

        var date = new Date(Number(timestamp) * 1000);

        var dontStop = false;
        var s = '';
        switch(true)    {
            default:
            case $(this).is('.localit-timestamp-datetime'):
                dontStop = true;
            case $(this).is('.localit-timestamp-date'):
                s = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + '<br />';
                if (!dontStop) break;

            case $(this).is('.localit-timestamp-time'):
                s += date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
                break;
        }
        $(this).html(s);
    })
})
