/**
 * Created by fliak on 11/11/13.
 */

$.prototype.serializeHash = function()  {
    var arr = $(this).serializeArray();
    var hash = {};
    for (var i in arr)  {
        hash[arr[i].name] = arr[i].value;
    }

    return hash;
};


var restApi = function(resource, method, config)    {
    config = $.extend(config, {
        type: method,
        url: 'localit/api/reference/' + resource
    });

    return $.ajax(config);
};

var newPopup = function(config)  {

    config = $.extend({}, config || {});

    var popupTemplate = config.template;

    var popup = $('<div class="col-xs-3 localit-popup"></div>');
    new EJS({url: popupTemplate}).update(popup[0], {})

    var that = this;

    popup.close = function()    {
        popup.find('form')[0].reset();
        popup.fadeOut();
    };

    popup.open = function(position)    {
        position.left = position.left - popup.width();

        popup.animate(position, 0);
        popup.fadeIn()
    };


    popup.find('[data-localit-action]').click(function()  {
        var action = $(this).attr('data-localit-action');

        switch (action) {
            case 'close-popup':
                popup.close();

                break;

            case 'post-language':

                var data = popup.find('form').serializeHash();

                restApi(that.bookName, 'post', {
                    data: data
                }).complete(function(xhr, status) {
                        if (status === 'success')   {
                            if (xhr.responseJSON)   {
                                var response = xhr.responseJSON;
                                if (response.success)   {
                                    console.log(that);
                                    $(that).append('<option value="' + response.entity.id + '">' + response.entity.name + '</option>');

                                    popup.close();
                                }
                                else    {
                                    throw response.message;
                                }
                            }
                            else    {
                                throw 'Answer malformed';
                            }
                        }
                        else    {
                            throw xhr.statusText;
                        }
                    });

                break;



        }
    })


    return popup;
};

jQuery.prototype.localitEditable = function()   {
    $(this).find('[data-localit-editable]').each(function() {
        var bookName = $(this).attr('data-localit-refbook');
        this.bookName = bookName;

        var template = $(this).attr('data-localit-template');
        if (!template)  {
            template = 'js/' + 'popup-edit-' + bookName + '.ejs';
        }

        var selectEl = this;


        var popup = newPopup.call(this, {
            template: template
        });
        $(selectEl).parents('.container').append(popup);


        var toolbar = $(this).siblings('.localit-bbar');
        if (!toolbar.length)    {
            var toolbar = $('<div class="localit-bbar"></div>');
            $(this).parents('.form-group').after(toolbar);
        }

        new EJS({url: 'js/bbar.ejs'}).update(toolbar[0], {})


        //Programming buttons

        toolbar.find('[data-localit-action]').click(function()  {
            var action = $(this).attr('data-localit-action');

            switch (action) {
                case 'remove':
                    var selectedElements = $(selectEl).val();
                    if (!Array.isArray(selectedElements))  {
                        selectedElements = [selectedElements];
                    }

                    if (!confirm('You sure want to remove ' + selectedElements.length + ' items?')) return false;

                    $.each(selectedElements, function(index, id) {
                        restApi(bookName + '/' + id, 'delete').complete(function(xhr, status) {
                            if (status === 'success')   {
                                if (xhr.responseJSON.success)   {
                                    $(selectEl).find('option[value=' + id + ']').remove();
                                }
                                else    {
                                    throw xhr.responseJSON.message;
                                }
                            }
                            else    {
                                throw xhr.statusText;
                            }
                        });
                    });

                    break;

                case 'add':
                    var position = $(this).offset();
                    popup.open(position);

                    break;
            }


        })


    });

};