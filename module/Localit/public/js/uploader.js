var FileUploadForm = function(action) {
    this.el = document.createElement('div');
    $(this.el).hide();
    $('body').append(this.el);

    this.action = action;
};

FileUploadForm.prototype.action = '';
FileUploadForm.prototype.el = null;

FileUploadForm.prototype.putData = function(data)  {
    data = {
        data: data || {},
        action: this.action
    };
    console.log(data)
    new EJS({url: 'js/file-upload-form.ejs'}).update(this.el, data)
};

FileUploadForm.prototype.uploadFile = function(data)  {

    this.putData(data);

    var form = $(this.el).find('form');
    console.log(form);
    var file = form.find('#file');
    file.change(function()  {
        form.submit();
    });
    file.click();


};