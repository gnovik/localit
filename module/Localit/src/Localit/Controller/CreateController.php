<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Localit\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Localit\Entity;
use Zend\Debug\Debug;
use Localit\Metric;
use Localit\Parser\Parser;

class CreateController extends AbstractActionController {
    use Traits\FetchDocumentManagerTrait;

    protected function getCurrentActionConfig()    {
        $currentAction = $this->params('action');

        $config = $this->getServiceLocator()->get('Config');
        $moduleConfig = array_key_exists('localit', $config) ? $config['localit'] : [];
        $masterConfig = array_key_exists('create_task_master', $moduleConfig) ? $moduleConfig['create_task_master'] : [];

        return array_key_exists($currentAction, $masterConfig) ? $masterConfig[$currentAction] : [];
    }

    protected function dispatchCurrent(ViewModel $viewModel)  {
        $currentConfig = $this->getCurrentActionConfig();

        if (array_key_exists('view', $currentConfig))   {
            $viewModel->setTemplate($currentConfig['view']);
        }

        $viewModel->setVariable('action', $this->params('action'));

        return $viewModel;
    }

    protected function dispatchNext($params = array())  {

        $currentConfig = $this->getCurrentActionConfig();

        if (!array_key_exists('next', $currentConfig))  {
            throw new \Exception('Config for this action is missing');
        }

        $currentConfig = $currentConfig['next'];

        $forwardType = $currentConfig['forwardType'];

        $params = array_merge(
            array_key_exists('routeParams', $currentConfig) ? $currentConfig['routeParams'] : [],
            $params
        );

        switch ($forwardType)   {
            case 'forward':
                return $this->forward()->dispatch($currentConfig['route'], $params);

            case 'redirect':
                //stringify entities
                foreach ($params as &$param) {
                    if (!is_scalar($param)) {
                        if (method_exists($param, 'getId')) {
                            $param = $param->getId();
                        }
                        else    {
                            $param = (string)$param;
                        }
                    }
                }

                return $this->redirect()->toRoute($currentConfig['route'], $params);

            default:
                throw new \Exception("Invalid forward type `$forwardType`, should be `forward` or `redirect`");

        }

    }


    public function uploadTaskFileAction()  {
        $view = new ViewModel();

        try {

            $form = $this->getServiceLocator()->get('localitUploadTaskFileForm');
            $view->setVariable('form', $form);

            $request = $this->getRequest();
            if ($request->isPost()) {
                // Make certain to merge the files info!
                $post = array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                );

                $form->setData($post);
                if ($form->isValid()) {
                    $data = $form->getData();

                    $tmpName  = $data['taskFile']['tmp_name'];
                    $taskName = $data['taskFile']['name'];
                    $mimeType = $data['taskFile']['type'];


                    $authService = $this->getServiceLocator()->get('zfcuser_auth_service');
                    if (!$authService)   {
                        throw new \Exception('Cannot fetch user');
                    }
                    $user = $authService->getIdentity();

                    $storeAsFile = $data['asFile'];

                    if ($storeAsFile)    {

                        $file = new Entity\File();
                        $file->setFile($tmpName);
                        $file->setMime($mimeType);

                        $file->setOriginName($taskName);

                        $this->getDM()->persist($file);
                        $this->getDM()->flush();

                        $task = new Entity\TaskFile();
                        $task->setOriginFile($file);
                        $task->setOwner($user);
                        $task->setName($taskName);
                        $this->getDM()->persist($task);
                        $this->getDM()->flush();

                        $dispatch = $this->redirect()->toRoute('localit/create-task-master', [
                            'action' => 'save-task-meta-info',
                            'task' => $task->getId()
                        ]);
                    }
                    else    {

                        $parser = Parser::getParserForFile($tmpName, $taskName, $mimeType, [
                            'removeBlankLines' => true
                        ]);

                        if (!$parser) {
                            throw new \Exception('This type of file cannot be parsed');
                        }
                        $parsed = $parser->parse(false);
                        if ($parsed !== true)   {
                            throw new \Exception('An error with file parsing');
                        }

                        $parsedData = $parser->getParsedData();

                        $task = new Entity\TaskInstant();
                        $task->setChunks($parsedData);
                        $task->setOwner($user);

                        $task->setName($taskName);
                        $this->getDM()->persist($task);
                        $this->getDM()->flush();

                        $dispatch = $this->dispatchNext([
                            'task' => $task
                        ]);
                    }


                    return $dispatch;
                }
            }
        }
        catch(\Exception $e)    {
            echo '<pre>';
            echo $e; exit();
            $view->setVariable('errorMessage', $e->getMessage());
        }

        return $this->dispatchCurrent($view);
    }


    public function approveExportResultAction() {
        $response = array();

        try {

            $form = $this->getServiceLocator()->get('localiteEditTaskContentForm');
            $response['form'] = $form;

            $request = $this->getRequest();
            if ($request->isPost()) {
                $post = $request->getPost();

                $taskId = $post->id;
                $response['task'] = $taskId;

                $dm = $this->getDM();
                $rep = $dm->getRepository('\Localit\Entity\TaskAbstract');
                $task = $rep->find($taskId);

                if (!is_null($post->cancel))    {
                    $dm->remove($task);
                    $dm->flush();

                    return $this->forward()->dispatch('Application\Controller\CreateTask', array(
                        'action' => 'index',
                        'step' => 1
                    ));
                }

                $response['task'] = $task;

                $form->bind($task);
                $data = $post->toArray();

                $removeItems = [];
                $length = 0;
                foreach ($data['chunks'] as $index => $chunk) {
                    if (strlen($chunk['origin']) === 0) {
                        $removeItems[] = $index;
                        continue;
                    }

                    $length += Metric\Text::getWordsCount($chunk['origin']);
                }

                foreach ($removeItems as $removeIndex)  {
                    unset($data['chunks'][$removeIndex]);
                }


                $form->setData($data);

                if ($form->isValid())   {
                    $task->setWordsCount($length);

                    $dm->persist($task);
                    $dm->flush();

                    return $this->dispatchNext(['task' => $task]);
                }

                $response['task'] = $task;

            }
            else    {
                $response['task'] = $this->params('task');
            }
        }
        catch(\Exception $e)    {
            $response['errorMessage'] = $e->getMessage();
        }

        return $this->dispatchCurrent(new ViewModel($response));
    }

    public function saveTaskMetaInfoAction()  {
        $view = new ViewModel();

        $form = $this->getServiceLocator()->get('localitCreateTaskForm');
        $view->setVariable('form', $form);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();

            $taskId = $post->id;

            $dm = $this->getDM();

            $rep = $dm->getRepository('Localit\Entity\TaskAbstract');
            if ($taskId)    {
                $task = $rep->find($taskId);

                if (!is_null($post->cancel))    {
                    $dm->remove($task);
                    $dm->flush();

                    return $this->forward()->dispatch('Application\Controller\CreateTask', array(
                        'action' => 'index',
                        'step' => 1
                    ));
                }
            }
            else    {
                $task = new Entity\Task();
            }



            $form->bind($task);

            $form->setData($post->toArray());

            if ($form->isValid())   {

                $task = $form->getData();

                $taskRep = $this->getServiceLocator()->get('TaskRepository');
                $nextTaskNumber = $taskRep->generateNextInstanceNumber();

                $task->publish($nextTaskNumber);
//                $task->publish(time());

                $dm->persist($task);
                $dm->flush();

                $eventManager = $this->getServiceLocator()->get('localitEventManager');
                $eventManager->trigger('create_task', $this, ['task' => $task]);

                return $this->dispatchNext(['task' => $task]);
            }
        }
        else    {
            $task = $this->params('task');
        }

        $view->setVariable('task', $task);



        return $this->dispatchCurrent($view);
    }


}