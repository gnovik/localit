<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/11/13
 * Time: 11:00 AM
 */

namespace Localit\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Localit\Entity;
use Zend\Debug\Debug;
use Localit\Metric;
use Zend\View\Model\JsonModel;

class EditController extends AbstractActionController {
    use Traits\FetchDocumentManagerTrait;

    public function indexAction()    {

    }


    public function saveTaskMetaInfoAction()    {
        //GET AND POST

        $view = new ViewModel();
        $request = $this->getRequest();

        if ($request->isPost()) {

            $form = $this->getServiceLocator()->get('localitCreateTaskForm');

            $post = $request->getPost();

            $config = $this->getServiceLocator()->get('Config');
            $moduleConfig = array_key_exists('localit', $config) ? $config['localit'] : [];
            $routesConfig = array_key_exists('routes', $moduleConfig) ? $moduleConfig['routes'] : [];


            if (!is_null($post->cancel))    {
                return $this->redirect()->toRoute($routesConfig['dashboard']);
            }

            $repo = $this->getServiceLocator()->get('TaskRepository');

            $postArr = $post->toArray();
            $originId = $postArr['id'];
            $originEntity = $repo->find($originId);

            if (!is_null($post->clone))    {
                unset($postArr['id']);

                $class = get_class($originEntity);
                $form->setObject(new $class);
            }
            else    {
                $form->setObject($originEntity);
            }



            $form->setData($postArr);

            if ($form->isValid())   {
                $entity = $form->getData();

                if (!is_null($post->clone))    {

                    $nextInstanceNumber = $repo->generateNextInstanceNumber();
                    $entity->setInstanceNumber($nextInstanceNumber);
                    $entity->copyEntity($originEntity);
                }
//                $entity->setDummyField(time());
                $this->getDM()->persist($entity);

                $this->getDM()->flush();

                return $this->redirect()->toRoute($routesConfig['dashboard']);
            }

            $view->setVariable('task', $post->id);

        }
        else    {
            $task = $this->params('task');
            $view->setVariable('task', $task);

        }

        return $view;
    }


    public function commentAction() {
        $request = $this->getRequest();

        if ($request->isPost()) {

            $post = $request->getPost();

            $repo = $this->getServiceLocator()->get('TaskRepository');

            $task = $this->params('task');
            if (is_scalar($task))   {
                $task = $repo->getByNumber($task);
            }

            $task->setComment($post->comment);

            $this->getDM()->flush($task);

            $config = $this->getServiceLocator()->get('Config');
            $moduleConfig = array_key_exists('localit', $config) ? $config['localit'] : [];
            $routesConfig = array_key_exists('routes', $moduleConfig) ? $moduleConfig['routes'] : [];

            return $this->redirect()->toRoute($routesConfig['translate-task'], [
                'task' => $task->getInstanceNumber()
            ]);
        }
    }

    public function recalcualteWordsCountAction()   {
        $view = new JsonModel();

        try {
            $task = $this->params('task');
            $repo = $this->getServiceLocator()->get('TaskRepository');

            if (is_scalar($task))   {
                $task = $repo->getByNumber($task);
            }

            if (!($task instanceof Entity\TaskInstant)) {
                throw new \Exception('Words for this kind of task cannot be recalculated');
            }

            $length = 0;
            foreach ($task->getChunks() as $chunk)  {
                $length += Metric\Text::getWordsCount($chunk['origin']);
            }

            $task->setWordsCount($length);

            $this->getDM()->persist($task);
            $this->getDM()->flush();

            $view->setVariable('success', true);
            $view->setVariable('wordsCount', $length);
        }
        catch (\Exception $e)   {
            $view->setVariable('success', false);
            $view->setVariable('message', $e->getMessage());
        }

        return $view;

    }

} 