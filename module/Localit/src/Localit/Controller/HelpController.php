<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/20/13
 * Time: 2:53 PM
 */

namespace Localit\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class HelpController extends AbstractActionController {
    public function explainAction() {
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);

        $status = $this->params('status');
        $viewModel->setTemplate('localit/status-description/' . $status);


        return $viewModel;

    }
} 