<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/11/13
 * Time: 3:47 PM
 */

namespace Localit\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Stdlib\Hydrator;

use Localit\Entity;

use Zend\Debug\Debug;

class ReferenceBookController extends AbstractRestfulController  {

    use Traits\FetchDocumentManagerTrait;

    protected function getEntityClass($book = false)    {
        if (!$book)  {
            $book = $this->params('book');
        }
        $book = ucfirst($book);

        $bookEntities = ['Language', 'Category'];

        if (in_array($book, $bookEntities)) {
            $class = '\Localit\Entity\\' . $book;
            if (class_exists($class))   {
                return $class;
            }
            else    {
                throw new \Exception("Entity class for book `$book` is missing");
            }
        }
        else    {
            throw new \Exception("Book `$book` is missing");
        }
    }

    protected function getRepository($book = false) {
        $class = $this->getEntityClass($book);

        return $this->getDM()->getRepository($class);
    }

    protected function getViewModel()   {
        $criteria = array(
            'Zend\View\Model\JsonModel' => array(
                '\*/json',
            ),
        );

        $viewModel = $this->acceptableViewModelSelector($criteria);

        if ($viewModel instanceof JsonModel) {
            $viewModel->setTerminal(true);
        }

        return $viewModel;
    }

    public function get($id)  {

        $rep = $this->getRepository();

        $entity = $rep->find($id);

        $viewModel = $this->getViewModel();
        $viewModel->setVariable('entity', $entity);

        return $viewModel;
    }

    public function getList()   {
        $rep = $this->getRepository();
        $list = $rep->findAll();

        $viewModel = $this->getViewModel();
        $viewModel->setVariable('list', $list);

        return $viewModel;
    }

    public function delete($id)    {
        $view = $this->getViewModel();

        try {
            $rep = $this->getRepository();
            $entity = $rep->find($id);
            $entity->setTrash(true);

            $this->getDM()->persist($entity);
            $this->getDM()->flush();

            $view->setVariable('success', true);

        }
        catch(\Exception $e)    {
            $view->setVariable('success', false);
            $view->setVariable('message', $e->getMessage());
        }

        return $view;
    }

    public function create($post)  {

        $view = $this->getViewModel();

        try {
            $entityClass = $this->getEntityClass();

            $entity = new $entityClass();

            $hydrator = new Hydrator\ClassMethods();
            $hydrator->hydrate($post, $entity);

            $this->getDM()->persist($entity);
            $this->getDM()->flush();

            $view->setVariable('success', true);
            $view->setVariable('entity', $entity);

        }
        catch(\Exception $e)    {
            $view->setVariable('success', false);
            $view->setVariable('message', $e->getMessage());
        }

        return $view;
    }

} 