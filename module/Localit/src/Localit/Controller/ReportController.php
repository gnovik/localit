<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/18/13
 * Time: 3:52 PM
 */

namespace Localit\Controller;

use Localit\Parser;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Localit\Entity;
use Zend\Debug\Debug;
use Localit\Metric;



class ReportController extends AbstractActionController {
    use Traits\FetchDocumentManagerTrait;

    public function indexAction()   {
        $idList = $this->params('idList');
        $ids = array_unique(explode(',', $idList));

        $filterLanguage = $this->params('language', false);

        $repo = $this->getServiceLocator()->get('TaskRepository');


        $phpExcel = new \PHPExcel();
        $phpExcel->getProperties()->setCreator("Localit Translation Software");
        $phpExcel->getProperties()->setLastModifiedBy("");
        $phpExcel->getProperties()->setTitle("Task Report");
        $phpExcel->getProperties()->setSubject("");
        $phpExcel->getProperties()->setDescription("Export Task Report");

        $phpExcel->setActiveSheetIndex(0);
        $sheet = $phpExcel->getActiveSheet();

        $sheet->getCellByColumnAndRow(0, 1)->setValue('Task Id');
        $sheet->getCellByColumnAndRow(1, 1)->setValue('Task Name');
        $sheet->getCellByColumnAndRow(2, 1)->setValue('Creation Date');
        $sheet->getCellByColumnAndRow(3, 1)->setValue('Expected Date');
        $sheet->getCellByColumnAndRow(4, 1)->setValue('Start Date');
        $sheet->getCellByColumnAndRow(5, 1)->setValue('Approved Date');
        $sheet->getCellByColumnAndRow(6, 1)->setValue('Category');
        $sheet->getCellByColumnAndRow(7, 1)->setValue('Words Count');
        $sheet->getCellByColumnAndRow(8, 1)->setValue('Language');
        $sheet->getCellByColumnAndRow(9, 1)->setValue('Rate');
        $sheet->getCellByColumnAndRow(10, 1)->setValue('Amount');
        $sheet->getCellByColumnAndRow(11, 1)->setValue('Status');


        $showWordsCountHelper = $this->getServiceLocator()->get('viewhelpermanager')->get('showWordsCount');

        $rowIndex = 2;
        foreach ($ids as $instanceNumber)   {
            $task = $repo->getByNumber($instanceNumber);

            foreach ($task->getTranslate() as $language)  {

                if ($filterLanguage && $filterLanguage != $language->getAbbr())  {
                    continue;
                }

                $sheet->getCellByColumnAndRow(0, $rowIndex)->setValue($task->getInstanceNumber());
                $sheet->getCellByColumnAndRow(1, $rowIndex)->setValue($task->getName());
                $sheet->getCellByColumnAndRow(2, $rowIndex)->setValue($task->getCreationDate('Y-m-d'));
                $sheet->getCellByColumnAndRow(3, $rowIndex)->setValue($task->getExpectedDate('Y-m-d'));
                $sheet->getCellByColumnAndRow(4, $rowIndex)->setValue($task->getStartDate('Y-m-d'));
                $sheet->getCellByColumnAndRow(5, $rowIndex)->setValue($task->getApprovedDate('Y-m-d'));
                $sheet->getCellByColumnAndRow(6, $rowIndex)->setValue((string)$task->getCategory());
                $sheet->getCellByColumnAndRow(7, $rowIndex)->setValue($showWordsCountHelper($task, ['short' => true]));

                $sheet->getCellByColumnAndRow(8, $rowIndex)->setValue($language->getAbbr('Y-m-d'));
                $sheet->getCellByColumnAndRow(9, $rowIndex)->setValue($language->getRate('Y-m-d'));
                $sheet->getCellByColumnAndRow(10, $rowIndex)->setValue($task->getSumForLanguage($language));

                $sheet->getCellByColumnAndRow(11, $rowIndex)->setValue($task->getStatusTitle('Y-m-d'));

                $rowIndex++;
            }
        }

        $sheet->setTitle('Report');

        $phpExcelWriter = new \PHPExcel_Writer_Excel2007($phpExcel);

        $fileName = '/tmp/phpexcel.xlsx';
        $phpExcelWriter->save($fileName);

        $outputFileName = "task_report" . date('Y-m-d') . ".xlsx";

        $response = $this->getResponse();
        $response->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => "attachment;filename=\"{$outputFileName}\"",
            'Cache-Control' => 'max-age=0',
        ));
        $response->setContent(file_get_contents($fileName));

        return $response;
    }

} 