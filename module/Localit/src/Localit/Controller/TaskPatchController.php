<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Localit\Controller;

use Localit\Parser;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Router\Http\TranslatorAwareTreeRouteStack;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

use Localit\Entity;
use Zend\Debug\Debug;
use Localit\Metric;
use Zend\Mvc\MvcEvent;

class TaskPatchController extends AbstractActionController {
    use Traits\FetchDocumentManagerTrait;

    protected function getTask()    {
        $task = $this->params('task');
        if (is_scalar($task))   {
            $task = $this->getServiceLocator()->get('TaskRepository')->getByNumber($task);
        }

        return $task;
    }

    public function status($newStatus)  {

        if (!array_key_exists($newStatus, Entity\TaskAbstract::$allowedStatuses))   {
            throw new \Exception("Status `$newStatus` is not available");
        }

        $task = $this->getTask();

        $currentStatus = $task->getStatus();

        if (!$this->isAllowed('set-task-status', $currentStatus . '_' . $newStatus))    {
            throw new \Exception("Not enough permissions to change status from `$currentStatus` to `$newStatus`");
        }


        $task->setStatus($newStatus);
        $this->getDM()->persist($task);
        $this->getDM()->flush();

        return true;
    }

    public function estimatedDate($date)   {

        $task = $this->getTask();
        $task->setEstimatedDate($date);

        $this->getDM()->persist($task);
        $this->getDM()->flush();

        return true;
    }

    public function quantityWordsUnique($count)   {

        $task = $this->getTask();
        $task->setQuantityWordsUnique($count);

        $this->getDM()->persist($task);
        $this->getDM()->flush();

        return true;
    }

    public function quantityWordsNonUnique($count)   {

        $task = $this->getTask();
        $task->setQuantityWordsNonUnique($count);

        $this->getDM()->persist($task);
        $this->getDM()->flush();

        return true;
    }

    public function onDispatch(MvcEvent $event)
    {
        $routeMatch = $event->getRouteMatch();
        if (! $routeMatch) {
            throw new \Zend\Mvc\Exception\DomainException(
                'Missing route matches; unsure how to retrieve action');
        }

        $action  = $routeMatch->getParam('action', false);
        if ($action) {
            // Handle arbitrary methods, ending in Action
            $method = static::getMethodFromAction($action);
            if (!method_exists($this, $method)) {
                $method = 'notFoundAction';
            }
            $return = $this->$method();
            $event->setResult($return);

            return $return;
        }

        $request = $event->getRequest();
        if (!$request->isPost())    {
            throw new \Exception('Only POST request available');
        }

        $post = $request->getPost()->toArray();

        $jsonModel = new JsonModel();
        $jsonModel->setVariable('success', true);

        try {
            foreach ($post as $propName => $propValue)  {
                $method = $propName;
                if (method_exists($this, $method))  {
                    $return = $this->$method($propValue);
                    $jsonModel->setVariable($propName, $return);
                }
                else    {
                    $jsonModel->setVariable('success', false);
                }
            }
        }
        catch(\Exception $e)    {
            $jsonModel->setVariable('success', false);
            $jsonModel->setVariable('message', $e->getMessage());
        }


        $routeMatch->setParam('action', $action);
        $event->setResult($jsonModel);

        return $jsonModel;
    }

}