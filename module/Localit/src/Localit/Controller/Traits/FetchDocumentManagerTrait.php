<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/6/13
 * Time: 10:27 AM
 */

namespace Localit\Controller\Traits;

use Doctrine\ODM\MongoDB\DocumentManager;
use Zend\Mvc\Controller\PluginManager;

trait FetchDocumentManagerTrait {


    /**
     * @var DocumentManager
     */
    protected $dm;

    /**
     * @return DocumentManager
     */
    protected function getDM()  {
        if (!$this->dm)  {
            $serviceLocator = $this->getServiceLocator();
            if ($serviceLocator instanceof PluginManager)   {
                $serviceLocator = $serviceLocator->getServiceLocator();
            }

            $this->dm = $serviceLocator->get('doctrine.documentmanager.odm_default');
        }

        return $this->dm;
    }

} 