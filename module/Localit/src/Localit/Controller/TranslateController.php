<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/18/13
 * Time: 3:52 PM
 */

namespace Localit\Controller;

use Localit\Parser;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Localit\Entity;
use Zend\Debug\Debug;
use Localit\Metric;



class TranslateController extends AbstractActionController {
    use Traits\FetchDocumentManagerTrait;

    public function uploadTranslationAction()   {
        $view = new ViewModel();

        $task = $this->params('task');

        try {

            if (is_scalar($task))   {
                $task = $this->getServiceLocator()->get('TaskRepository')->find($task);
            }

            $request = $this->getRequest();
            if (!$request->isPost())    {
                throw new \Exception('Only POST request available');
            }

            $form = $this->getServiceLocator()->get('localitUploadTranslationForm');

            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );

            $form->setData($post);

            if ($form->isValid()) {
                $data = $form->getData();

                $langAbbr = $data['language'];

                $tmpName  = $data['file']['tmp_name'];
                $taskName = $data['file']['name'];
                $mimeType = $data['file']['type'];

                if ($task instanceof Entity\TaskFile)   {
                    $file = new Entity\File();
                    $file->setFile($tmpName);
                    $file->setMime($mimeType);

                    $file->setOriginName($taskName);

                    $this->getDM()->persist($file);
                    $this->getDM()->flush();

                    $task->addTranslatedFile($langAbbr, $file);

                    $language = $this->getDM()->getRepository('\Localit\Entity\Language')
                        ->findOneBy(['abbr' => $langAbbr]);

                    if (!$language) {
                        throw new \Exception("Language `$langAbbr` is missing");
                    }
                    $task->markAsFinished($language);

                    $this->getDM()->persist($task);
                    $this->getDM()->flush();

                    $eventManager = $this->getServiceLocator()->get('localitEventManager');
                    $eventManager->trigger('upload_translation', $this, ['task' => $task, 'language' => $langAbbr]);

                    /**
                     * FIXME use dependency for route
                     */
                    return $this->redirect()->toRoute('application/translate-task', [
                        'task' => $task->getInstanceNumber()
                    ]);

                }
                else    {

                    $parser = new Parser\Xls($tmpName, [
                        'removeBlankLines' => true,
                        'translateLanguage' => $langAbbr
                    ]);
                    $parsed = $parser->parse(true);
                    if ($parsed !== true)   {
                        throw new \Exception('An error with file parsing');
                    }

                    $parsedData = $parser->getParsedData();

                    $task->setTmpRepo([
                        'savedAt' => date('Y-m-d H:i:s'),
                        'translation' => $parsedData,
                        'language' => $langAbbr
                    ]);
                }

                $this->getDM()->persist($task);
                $this->getDM()->flush();


                $this->redirect()->toRoute('localit/approve-translation', ['task' => $task->getInstanceNumber()]);

            }

            $errorMessages = $form->getMessages();
        }
        catch(\Exception $e)    {
            $errorMessages = [$e->getMessage()];
        }

        /**
         * @FIXME use dependency
         */

        return $this->forward()->dispatch('Application\Controller\Task', [
            'action' => 'translate',
            'task' => $task,
            'messages' => [$this->params('action') => $errorMessages]
        ]);

    }

    public function approveTranslationAction()    {
        $view = new ViewModel();

        $task = $this->params('task');

        try {

            if (is_scalar($task))   {
                $task = $this->getServiceLocator()->get('TaskRepository')->getByNumber($task);
            }

            $view->setVariable('options', []);
            $view->setVariable('task', $task);

            $form = $this->getServiceLocator()->get('localiteApproveTranslationContentForm');
            $view->setVariable('form', $form);



            $request = $this->getRequest();
            if ($request->isPost())    {
                $post = $request->getPost()->toArray();

                $form->initialize($post['chunks'], 'chunks', true);
                if (array_key_exists('extra', $post))   {
                    $form->initialize($post['extra'], 'extra');
                }

                $form->setData($post);

                if ($form->isValid())   {
                    $data = $form->getData();

                    $langAbbr = $data['language'];

                    $originChunks = $task->getChunks();
                    $originIndex = [];
                    foreach ($originChunks as $key => $pair)  {
                        $originIndex[$pair['origin']] = $key;
                    }

                    foreach ($data['chunks'] as $pair)  {
                        $index = $originIndex[$pair['origin']];
                        $originChunks[$index][$langAbbr] = $pair[$langAbbr];
                    }

                    if (array_key_exists('includeExtraFields', $data) && $data['includeExtraFields'])   {
                        $resultChunks = array_merge($originChunks, $data['extra']);
                    }
                    else    {
                        $resultChunks = $originChunks;
                    }

                    $task->setChunks($resultChunks);
                    $task->emptyTmpRepo();

                    $language = $this->getDM()->getRepository('\Localit\Entity\Language')->findOneBy(['abbr' => $langAbbr]);
                    if (!$language) {
                        throw new \Exception("Language `$langAbbr` is missing");
                    }
                    $task->markAsFinished($language);


                    $this->getDM()->persist($task);
                    $this->getDM()->flush();

                    $eventManager = $this->getServiceLocator()->get('localitEventManager');
                    $eventManager->trigger('upload_translation', $this, ['task' => $task, 'language' => $langAbbr]);

                    /**
                     * FIXME use dependency for route
                     */
                    return $this->redirect()->toRoute('application/translate-task', [
                        'task' => $task->getInstanceNumber()
                    ]);
                }

                $view->setTemplate('localit/translate/approve-parse-result');
            }
            else    {

                $tmpTanslationData = $task->getTmpRepo();
                $parsedData = $tmpTanslationData['translation'];
                $language = $tmpTanslationData['language'];

                $translatedIndex = [];
                foreach ($parsedData as $key => $pair)  {
                    $translatedIndex[$pair['origin']] = $key;
                }

                $forApprove = [];
                $originChunks = $task->getChunks();

                foreach ($originChunks as &$pair) {
                    $originSentence = $pair['origin'];
                    $forApprovePair = [
                        'origin'  => $originSentence
                    ];

                    if (array_key_exists($originSentence, $translatedIndex))    {
                        $index = $translatedIndex[$originSentence];
                        $forApprovePair[$language] = $parsedData[$index][$language];

                        //remove to hydrate extra fields
                        unset($parsedData[$index]);
                    } else    {
                        $forApprovePair[$language] = '';
                    }

                    $forApprove[] = $forApprovePair;
                }


                $form->initialize($forApprove, 'chunks', true);
                $form->initialize($parsedData, 'extra');
                $form->get('language')->setValue($tmpTanslationData['language']);

            }
        }
        catch(\Exception $e)    {
            $errorMessages = [$e->getMessage()];
            $view->setVariable('errorMessages', $errorMessages);
        }

        $view->setTemplate('localit/translate/approve-parse-result');

        return $view;
    }

    public function downloadTranslationAction() {
        $task = $this->params('task');
        $language = $this->params('language');

        try {

            if (is_scalar($task))   {
                $task = $this->getServiceLocator()->get('TaskRepository')->getByNumber($task);
            }

            if ($language !== 'origin' && !$task->isTranslationFinished($language))    {
                throw new \Exception("Translation for $language language can't be exported because it donesn't finished yet");
            }

            $taskName = $task->getName();

            if ($task instanceof Entity\TaskFile)   {
                if ($language === 'origin') {
                    $file = $task->getOriginFile();
                }
                else    {
                    $file = $task->getTranslatedFile($language);
                }
                $fileExtension = $file->getFileExtension();
                $outputFileName = "translation_{$taskName}_{$language}.{$fileExtension}";

                return $this->fileOutput($file, [
                    'fileName' => $outputFileName
                ]);
            }



            $phpExcel = new \PHPExcel();
            $phpExcel->getProperties()->setCreator("Localit Translation Software");
            $phpExcel->getProperties()->setLastModifiedBy("");
            $phpExcel->getProperties()->setTitle("Translation $language for {$taskName} task");
            $phpExcel->getProperties()->setSubject("Export Translation Document");
            $phpExcel->getProperties()->setDescription("Export Translation Document");

            $phpExcel->setActiveSheetIndex(0);
            $sheet = $phpExcel->getActiveSheet();

            $chunks = $task->getChunks();

            $rowIndex = 1;
            foreach ($chunks as $chunk) {

                if (array_key_exists('origin', $chunk))    {
                    $sheet->getCellByColumnAndRow(0, $rowIndex)->setValue($chunk['origin']);
                }

                if ($language !== 'origin' && array_key_exists($language, $chunk))    {
                    $sheet->getCellByColumnAndRow(1, $rowIndex)->setValue($chunk[$language]);
                }


                $rowIndex++;
            }

            $sheet->setTitle($language);

            $phpExcelWriter = new \PHPExcel_Writer_Excel2007($phpExcel);

            $fileName = '/tmp/phpexcel.xlsx';
            $phpExcelWriter->save($fileName);

            $outputFileName = "translation_{$taskName}_{$language}.xlsx";

            $response = $this->getResponse();
            $response->getHeaders()->addHeaders(array(
                'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'Content-Disposition' => "attachment;filename=\"{$outputFileName}\"",
                'Cache-Control' => 'max-age=0',
            ));
            $response->setContent(file_get_contents($fileName));

            return $response;
        }
        catch (\Exception $e)   {
            throw $e;
        }
    }

} 