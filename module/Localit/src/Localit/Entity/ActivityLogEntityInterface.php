<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/15/13
 * Time: 4:21 PM
 */

namespace Localit\Entity;


interface ActivityLogEntityInterface {

    public function getLog();
    public function setLog($arr);
    public function appendLog(LogEntry $logEntry);

    public static function getLogConfig();

}