<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 12/3/13
 * Time: 5:00 PM
 */

namespace Localit\Entity;


trait ActivityLogEntityTrait {

    /**
     * @param ArrayCollection $log
     */
    public function setLog($log)
    {
        $this->log = $log;
    }

    /**
     * @return ArrayCollection
     */
    public function getLog()
    {
        return $this->log;
    }


    public function appendLog(LogEntry $logEntry)   {
        $log = $this->getLog();
        $log->add($logEntry);
    }
} 