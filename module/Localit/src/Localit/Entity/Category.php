<?php
namespace Localit\Entity;

use Doctrine\MongoDB\IteratorAggregate;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(db="localit", collection="category")
 */
class Category extends EntityAbstract implements \IteratorAggregate   {

    /** @ODM\Id */
    public $id;

    /**
     * @ODM\String
     */
    public $name;


    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    public function getIterator() {
        return new \ArrayIterator(get_object_vars($this));
    }


    public function __toString()    {
        return $this->getName();
    }

}