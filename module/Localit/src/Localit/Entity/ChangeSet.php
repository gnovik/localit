<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/15/13
 * Time: 5:03 PM
 */

namespace Localit\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class ChangeSet {

    /**
     * @ODM\String
     */
    protected $type;

}