<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 10/31/13
 * Time: 1:17 PM
 */

namespace Localit\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\MappedSuperclass */
class EntityAbstract {

    /**
     * @var bool
     * @ODM\Boolean
     */
    protected $trash;

    /**
     * @var string
     * @ODM\String
     */
    protected $type;

    /**
     * @var int
     * @ODM\Int
     */
    protected $instanceCounter;


    /**
     * @param boolean $trash
     */
    public function setTrash($trash)
    {
        $this->trash = $trash;
    }

    /**
     * @return boolean
     */
    public function getTrash()
    {
        return $this->trash;
    }


    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * @param int $instanceCounter
     */
    public function setInstanceCounter($instanceCounter)
    {
        $this->instanceCounter = $instanceCounter;
    }

    /**
     * @return int
     */
    public function getInstanceCounter()
    {
        return $this->instanceCounter;
    }




}