<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 1/9/14
 * Time: 12:04 PM
 */

namespace Localit\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;


/**
 * @ODM\Document(db="localit", collection="file")
 */
class File {
    /** @ODM\Id */
    private $id;

    /** @ODM\Field */
    private $name;

    /** @ODM\File */
    private $file;

    /** @ODM\Field */
    private $uploadDate;

    /** @ODM\Field */
    private $length;

    /** @ODM\Field */
    private $chunkSize;

    /** @ODM\Field */
    private $md5;

    /** @ODM\Field */
    private $mime;

    /** @ODM\Field */
    private $originName;

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @param mixed $mime
     */
    public function setMime($mime)
    {
        $this->mime = $mime;
    }

    /**
     * @return mixed
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * @param mixed $originName
     */
    public function setOriginName($originName)
    {
        $this->originName = $originName;
    }

    /**
     * @return mixed
     */
    public function getOriginName()
    {
        return $this->originName;
    }

    public function getFileExtension()  {
        $fileOriginName = $this->getOriginName();
        return substr($fileOriginName, strrpos($fileOriginName, '.') + 1);
    }




} 