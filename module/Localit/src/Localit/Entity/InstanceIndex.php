<?php
namespace Localit\Entity;

use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(db="localit", collection="task")
 */
class InstanceIndex extends EntityAbstract   {

    /** @ODM\Id */
    public $id;

    /**
     * @var int
     * @ODM\Int
     */
    protected $instanceCounter;


    /**
     * @var string
     * @ODM\String
     */
    protected $type = 'service';



    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $instanceCounter
     */
    public function setInstanceCounter($instanceCounter)
    {
        $this->instanceCounter = $instanceCounter;
    }

    /**
     * @return int
     */
    public function getInstanceCounter()
    {
        return $this->instanceCounter;
    }

}