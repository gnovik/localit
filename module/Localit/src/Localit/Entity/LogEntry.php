<?php
namespace Localit\Entity;

use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class LogEntry extends EntityAbstract   {
    const TYPE_COMMENT = 'comment';
    const TYPE_PATCH = 'patch';
    const TYPE_CREATE = 'create';
    const TYPE_REMOVE = 'remove';
    const TYPE_PROCESS = 'process'; //fit for notification

    /**
     * @todo use local alias for target document
     * @ODM\ReferenceOne(targetDocument="\Application\Entity\User")
     */
    protected $user;


    /**
     * @ODM\Date
     */
    protected $date;


    /**
     * @ODM\Hash
     */
    protected $changeSet;

    /**
     * @ODM\String
     */
    protected $type;

    /**
     * @ODM\String
     */
    protected $comment;


    /**
     * @param mixed $changeSet
     */
    public function setChangeSet($changeSet)
    {
        $this->changeSet = $changeSet;
    }

    /**
     * @return mixed
     */
    public function getChangeSet()
    {
        if (!$this->changeSet)   {
            return [];
        }

        return $this->changeSet;
    }

    /**
     * @param mixed $timestamp
     */
    public function setDate($timestamp)
    {
        $this->date = $timestamp;
    }

    /**
     * @return mixed
     */
    public function getDate($format = false)
    {
        if ($format === false)  {
            return $this->date;
        }
        else    {
            if (is_object($this->date) && $this->date instanceof \DateTime) {
                return $this->date->format($format);
            }
            else    {
                return '';
            }
        }
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $message
     */
    public function setComment($message)
    {
        $this->comment = $message;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }



    public function __construct()   {
        $this->setDate(new \DateTime());
    }

}