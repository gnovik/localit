<?php
namespace Localit\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\Document(db="localit", collection="notification") */
class Notification extends EntityAbstract implements ActivityLogEntityInterface   {
    use ActivityLogEntityTrait;

    const NOTIFICATION_STATUS_WAITING = 'waiting';
    const NOTIFICATION_STATUS_TRYSEND = 'trysend';
    const NOTIFICATION_STATUS_SENT = 'sent';
    const NOTIFICATION_STATUS_ERROR = 'error';


    /** @ODM\Id */
    protected $id;


    /** @ODM\String */
    protected $status;

    /** @ODM\String */
    protected $from;

    /** @ODM\String */
    protected $subject;


    /** @ODM\String */
    protected $body;

    /** @ODM\Date(name="creation_date") */
    protected $creationDate;

    /** @ODM\Date(name="sent_date") */
    protected $sentDate;

    /**
     * @var ArrayCollection
     * @ODM\EmbedMany(targetDocument="LogEntry")
     */
    protected $log;

    /**
     * @var Array
     * @ODM\Collection
     */
    protected $subscribers;

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }



    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subscribers
     */
    public function setSubscribers($subscribers)
    {
        $this->subscribers = $subscribers;
    }

    /**
     * @return mixed
     */
    public function getSubscribers()
    {
        return $this->subscribers;
    }








    /**
     * @param mixed $creationDate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return mixed
     */
    public function getCreationDate($format = false)
    {
        if ($format === false)  {
            return $this->creationDate;
        }
        else    {
            if (is_object($this->creationDate) && $this->creationDate instanceof \DateTime) {
                return $this->creationDate->format($format);
            }
            else    {
                return '';
            }

        }
    }


    public function __construct()   {
        $this->setCreationDate(new \DateTime());
        $this->setStatus(self::NOTIFICATION_STATUS_WAITING);
        $this->setSubscribers(new ArrayCollection());
        $this->setLog(new ArrayCollection());
    }


    public static function getLogConfig()  {
        return [
            'fields' => [
                'status' => true
            ]
        ];
    }

}