<?php
namespace Localit\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(db="localit", collection="task", repositoryClass="\Localit\Repository\ExRepository")
 * @ODM\InheritanceType("SINGLE_COLLECTION")
 * @ODM\DiscriminatorField(fieldName="taskType")
 * @ODM\DiscriminatorMap({"file"="Localit\Entity\TaskFile", "instant"="Localit\Entity\TaskInstant"})
 */
class TaskAbstract extends EntityAbstract implements ActivityLogEntityInterface   {
    use ActivityLogEntityTrait;

    const STATUS_OPEN = 'open';
    const STATUS_IN_PROGRESS = 'inprogress';
    const STATUS_DONE = 'done';
    const STATUS_REOPENED = 'reopened';
    const STATUS_APPROVED = 'approved';
    const STATUS_CLOSED = 'closed';


    public static $allowedStatuses = [
        self::STATUS_OPEN        => 'Opened',
        self::STATUS_IN_PROGRESS => 'In Progress',
        self::STATUS_DONE        => 'Done',
        self::STATUS_REOPENED    => 'Reopened',
        self::STATUS_APPROVED    => 'Approved',
        self::STATUS_CLOSED      => 'Closed'
    ];


    /** @ODM\Id */
    protected $id;


    /** @ODM\String */
    protected $instanceNumber;


    /** @ODM\Boolean */
    protected $published;

    /** @ODM\Boolean */
    protected $archived;



    /** @ODM\Field(type="string") */
    protected $name;

    /** @ODM\Field(type="string") */
    protected $description;

    /**
     * @var ArrayCollection
     * @ODM\ReferenceMany(targetDocument="\Localit\Entity\Language")
     */
    protected $finished = [];

    /** @ODM\Date(name="creation_date") */
    protected $creationDate;

    /** @ODM\Date(name="expected_date") */
    protected $expectedDate;

    /** @ODM\Date(name="estimated_date") */
    protected $estimatedDate;


    /** @ODM\Date(name="approved_date") */
    protected $approvedDate;

    /** @ODM\Date(name="start_date") */
    protected $startDate = null;


    /** @ODM\String */
    private $status;


    /**
     * @var \Localit\Entity\Category
     * @ODM\ReferenceOne(targetDocument="\Localit\Entity\Category")
     */
    protected $category;

    /**
     * @var integer
     * @ODM\Int(name="words_count")
     */
    protected $wordsCount;


    /**
     * @var ArrayCollection
     * @ODM\EmbedMany(targetDocument="LogEntry")
     */
    protected $log;

    /**
     * @var array
     * @ODM\Hash
     */
    protected $tmpRepo;


    /**
     * @var string
     * @ODM\String(name="comment")
     */
    protected $comment;


    /**
     * @var ArrayCollection
     * @ODM\ReferenceMany(targetDocument="\Localit\Entity\Language")
     */
    protected $translate = [];

    /**
     * @var \Application\Entity\User
     * @todo use local alias for target document
     * @ODM\ReferenceOne(targetDocument="\Application\Entity\User")
     */
    protected $owner;




    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }


    /**
     * @param mixed $creationDate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return mixed
     */
    public function getCreationDate($format = false)
    {
        if ($format === false)  {
            return $this->creationDate;
        }
        else    {
            if (is_object($this->creationDate) && $this->creationDate instanceof \DateTime) {
                return $this->creationDate->format($format);
            }
            else    {
                return '';
            }

        }
    }


    /**
     * @param mixed $expectedDate
     */
    public function setExpectedDate($expectedDate)
    {
        $this->expectedDate = $expectedDate;
    }

    /**
     * @return mixed
     */
    public function getExpectedDate($format = false)
    {
        if ($format === false)  {
            return $this->expectedDate;
        }
        else    {
            if (is_object($this->expectedDate) && $this->expectedDate instanceof \DateTime) {
                return $this->expectedDate->format($format);
            }
            else    {
                return '';
            }

        }
    }

    /**
     * @param mixed $estimatedDate
     */
    public function setEstimatedDate($estimatedDate)
    {
        $this->estimatedDate = $estimatedDate;
    }

    /**
     * @return mixed
     */
    public function getEstimatedDate($format = false)
    {
        if ($format === false)  {
            return $this->estimatedDate;
        }
        else    {
            if (is_object($this->estimatedDate) && $this->estimatedDate instanceof \DateTime) {
                return $this->estimatedDate->format($format);
            }
            else    {
                return '';
            }
        }
    }



    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getStartDate($format = false)
    {
        if ($format === false)  {
            return $this->startDate;
        }
        else    {
            if (is_object($this->startDate) && $this->startDate instanceof \DateTime) {
                return $this->startDate->format($format);
            }
            else    {
                return '';
            }
        }
    }

    /**
     * @param mixed $approvedDate
     */
    public function setApprovedDate($approvedDate)
    {
        $this->approvedDate = $approvedDate;
    }

    /**
     * @return mixed
     */
    public function getApprovedDate($format = false)
    {
        if ($format === false)  {
            return $this->approvedDate;
        }
        else    {
            if (is_object($this->approvedDate) && $this->approvedDate instanceof \DateTime) {
                return $this->approvedDate->format($format);
            }
            else    {
                return '';
            }
        }
    }




    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        switch($status) {
            case self::STATUS_IN_PROGRESS:
                //set start date when status changed at first
                if (!$this->getStartDate())   {
                    $this->setStartDate(new \DateTime());
                }
                break;

            case self::STATUS_APPROVED:
                //set approved date when status changed
                $this->setApprovedDate(new \DateTime());

                break;

        }


        $this->status = $status;

    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusTitle()    {
        if (array_key_exists($this->status, self::$allowedStatuses))    {
            return self::$allowedStatuses[$this->status];
        }
        else    {
            return null;
        }
    }




    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @param mixed $taskNumber
     */
    public function setInstanceNumber($taskNumber)
    {
        $this->instanceNumber = $taskNumber;
    }

    /**
     * @return mixed
     */
    public function getInstanceNumber()
    {
        return $this->instanceNumber;
    }



    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @param array $finished
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;
    }

    /**
     * @return array
     */
    public function getFinished()
    {
        return $this->finished;
    }

    public function getFinishedHash()   {
        $languages = $this->getTranslateAbbr();
        $count = count($languages);
        if ($count === 0)   {
            return [];
        }

        $hash = array_combine($languages, array_fill(0, $count, false));

        foreach ($this->finished as $language)  {
            $hash[$language->getAbbr()] = true;
        }

        return $hash;
    }

    /**
     * @param string|Language $lang Set language abbreviate or language entity
     */
    public function isTranslationFinished($lang)   {
        if (is_object($lang) && $lang instanceof Language)   {
            $lang = $lang->getAbbr();
        }
        elseif (!is_string($lang))  {
            throw new \Exception('Lang should be Language entity or language abbreviate such as `en`');
        }


        foreach ($this->finished as $language)  {
            if ($language->getAbbr() === $lang) {
                return true;
            }
        }

        return false;
    }

    public function isAllTranslationsFinished()    {
        return ($this->finished->count() === $this->translate->count());
    }


    public function markAsFinished(Language $language) {
        foreach ($this->finished as $finishedLanguage)  {
            if ($finishedLanguage->getId() === $language->getId())  {
                return false;
            }
        }
        $this->finished->add($language);

        return true;
    }



    /**
     * @param int $wordsCount
     */
    public function setWordsCount($wordsCount)
    {
        $this->wordsCount = $wordsCount;
    }

    /**
     * @return int
     */
    public function getWordsCount()
    {
        return $this->wordsCount;
    }

    /**
     * @param mixed $archived
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;
    }

    /**
     * @return mixed
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * @param mixed $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param array $tmpData
     */
    public function setTmpRepo($tmpData)
    {
        $this->tmpRepo = $tmpData;
    }

    /**
     * @return array
     */
    public function getTmpRepo()
    {
        return $this->tmpRepo;
    }

    public function emptyTmpRepo()  {
        $this->tmpRepo = null;
    }



    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $needTranslateTo
     */
    public function setTranslate($needTranslateTo)
    {
        $this->translate = $needTranslateTo;
    }

    /**
     * @return mixed
     */
    public function getTranslate()
    {
        return $this->translate;
    }

    public function getTranslateAbbr()  {
        $arr = [];
        foreach ($this->translate as $language)  {
            $arr[] = $language->getAbbr();
        }

        return $arr;
    }

    public function addTranslate($language)  {
        $this->translate[] = $language;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }









    public function publish($taskNumber) {
        $this->setStatus(self::STATUS_OPEN);
        $this->setPublished(true);
        $this->setInstanceNumber($taskNumber);

    }


    public function getSumForLanguage($language)    {
        if ($this instanceof TaskFile)  {
            $uniqueWordsCount = $this->getQuantityWordsUnique();
            $nonUniqueWordsCount = $this->getQuantityWordsNonUnique();

            $sum = $uniqueWordsCount * $language->getRate();
            $sum += $nonUniqueWordsCount * $language->getRate() * 0.3; //discount!
        }
        else    {
            $wordsCount = $this->getWordsCount();
            $sum = $wordsCount * $language->getRate();
        }

        return $sum;
    }

    public function getTotalSum()   {
        $sum = 0;
        foreach ($this->getTranslate() as $language)    {
            $sum += $this->getSumForLanguage($language);
        }

        return $sum;
    }




    public function __construct()   {
        $this->setCreationDate(new \DateTime());
        $this->translate = new ArrayCollection();
        $this->finished  = new ArrayCollection();
        $this->log = new ArrayCollection();
    }


    public function copyEntity(TaskAbstract $origin) {
        $this->setArchived(false);
        $this->setTrash(false);
        $this->setPublished(true);

        $this->setWordsCount($origin->getWordsCount());


        $languages = $this->getTranslateAbbr();
        foreach ($origin->getFinished() as $language)   {
            if (in_array($language->getAbbr(), $languages)) {
                $this->markAsFinished($language);
            }
        }

        $this->setOwner($origin->getOwner());

        $this->copyBody($origin);

    }



    public static function getLogConfig()  {
        return [
            'fields' => [
                'name' => true,
                'description' => true,
                'translate' => true,
                'finished' => true,
                'expectedDate' => true,
                'estimatedDate' => true,
                'startDate' => true,
                'status' => true,
                'category' => true,
                'wordsCount' => true,
                'comment' => false
            ],
            'commentField' => 'comment'
        ];
    }

}