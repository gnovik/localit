<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 1/9/14
 * Time: 11:59 AM
 */

namespace Localit\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;



/**
 * @ODM\Document
 */
class TaskFile extends TaskAbstract {




    /**
     *
     * @var array
     * @ODM\ReferenceOne(targetDocument="Localit\Entity\File")
     */
    protected $originFile;


    /**
     *
     * @var array
     * @ODM\ReferenceMany(targetDocument="Localit\Entity\File")
     */
    protected $translatedFiles;

    /**
     * @var array
     * @ODM\Hash
     */
    protected $translateMap;

    /**
     *
     * @var integer
     * @ODM\Int(name="quantity_words_unique")
     */
    protected $quantityWordsUnique;

    /**
     *
     * @var integer
     * @ODM\Int(name="quantity_words_nonunique")
     */
    protected $quantityWordsNonUnique;

    /**
     * @param $originFile
     */
    public function setOriginFile($originFile)
    {
        $this->originFile = $originFile;
    }

    /**
     * @return array
     */
    public function getOriginFile()
    {
        return $this->originFile;
    }

    /**
     * @param array $translatedFiles
     */
    public function setTranslatedFiles($translatedFiles)
    {
        $map = [];
        $this->translatedFiles = $translatedFiles;

        $index = 0;
        foreach ($this->translatedFiles as $langAbbr => $file)  {
            $map[$langAbbr] = $index;
            $index++;
        }

        $this->translateMap = $map;
        $this->translatedFiles = $translatedFiles;
    }

    /**
     * @return array
     */
    public function getTranslatedFiles()
    {
        $translatedFiles = [];
        foreach ($this->translateMap as $langAbbr => $fileIndex)    {
            $translatedFiles[$langAbbr] = $this->translatedFiles[$fileIndex];
        }

        return $translatedFiles;
    }

    public function addTranslatedFile($languageAbbr, File $file) {
        $translatedFiles = $this->getTranslatedFiles();

        if (array_key_exists($languageAbbr, $translatedFiles))   {
            unset($translatedFiles[$languageAbbr]);
        }

        $translatedFiles[$languageAbbr] = $file;
        $this->setTranslatedFiles($translatedFiles);
    }

    public function getTranslatedFile($langAbbr)    {
        if (array_key_exists($langAbbr, $this->translateMap))   {
            $index = $this->translateMap[$langAbbr];
            return $this->translatedFiles[$index];
        }
        else    {
            return null;
        }
    }

    /**
     * @param int $quantityWordsNonUnique
     */
    public function setQuantityWordsNonUnique($quantityWordsNonUnique)
    {
        $this->quantityWordsNonUnique = $quantityWordsNonUnique;
    }

    /**
     * @return int
     */
    public function getQuantityWordsNonUnique()
    {
        return $this->quantityWordsNonUnique;
    }

    /**
     * @param int $quantityWordsUnique
     */
    public function setQuantityWordsUnique($quantityWordsUnique)
    {
        $this->quantityWordsUnique = $quantityWordsUnique;
    }

    /**
     * @return int
     */
    public function getQuantityWordsUnique()
    {
        return $this->quantityWordsUnique;
    }




    public function __construct()   {
        $this->translatedFiles = array();
//        $this->type = "file";

        parent::__construct();
    }

    protected function copyBody(TaskFile $origin)    {
        $this->setOriginFile($origin->getOriginFile());
        $this->setTranslatedFiles($origin->getTranslatedFiles());
        $this->setQuantityWordsUnique($origin->getQuantityWordsUnique());
        $this->setQuantityWordsNonUnique($origin->getQuantityWordsNonUnique());
    }


} 