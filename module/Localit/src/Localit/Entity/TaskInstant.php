<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 1/9/14
 * Time: 11:59 AM
 */

namespace Localit\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;


/**
 * @ODM\Document
 */
class TaskInstant extends TaskAbstract {

    /**
     *
     * @var array
     * @ODM\Collection
     */
    protected $chunks;


    /**
     * @param array $chunks
     */
    public function setChunks($chunks)
    {
        $this->chunks = $chunks;
    }

    /**
     * @return array
     */
    public function getChunks()
    {
        return $this->chunks;
    }

    protected function copyBody(TaskInstant $origin)    {
        $chunks = [];
        $languages = $this->getTranslateAbbr();

        foreach ($origin->getChunks() as $chunk)    {
            $newChunk = [
                'origin' => $chunk['origin']
            ];
            foreach ($chunk as $abbr => $sentence)  {
                if (in_array($abbr, $languages)) {
                    $newChunk[$abbr] = $sentence;
                }
            }

            $chunks[] = $newChunk;
        }

        $this->setChunks($chunks);
    }


} 