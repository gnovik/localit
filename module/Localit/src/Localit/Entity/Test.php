<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/13/13
 * Time: 11:38 AM
 */

namespace Localit\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;


/**
 * Class Test
 * @package Localit\Entity
 *
 * @ODM\EmbeddedDocument
 *
 */
class Test {

    /** @ODM\String */
    private $testValue;

    /**
     * @param mixed $testValue
     */
    public function setTestValue($testValue)
    {
        $this->testValue = $testValue;
    }

    /**
     * @return mixed
     */
    public function getTestValue()
    {
        return $this->testValue;
    }



} 