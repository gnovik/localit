<?php

namespace Localit\Form;

use Zend\Form\Element;
use Zend\Form;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Persistence\ProvidesObjectManager;

use Doctrine\Common\Persistence\ObjectManager;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\InputFilter\InputFilter;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Localit\Entity;


/**
 * Class CreateTask
 */
class CreateTask extends Form\Form implements ObjectManagerAwareInterface, InputFilterProviderInterface {
    use ProvidesObjectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function __construct($objectManager)   {
        parent::__construct('create_task');

        $this->setObjectManager($objectManager);

        $this->setHydrator(new DoctrineObject($objectManager, 'Localit\Entity\TaskAbstract', false));

        $this->setInputFilter(new InputFilter());


        $this->add(array(
            'type' => 'hidden',
            'name' => 'id'
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'        => 'text',
                'placeholder' => 'Task 1',
                'class'       => 'form-control',
                'required'    => true
            ),
            'options' => array(
                'label' => 'Task name',
                'label_attributes' => array(
                    'class' => 'control-label'
                ),
                'required' => true
            )
        ));


        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type'  => 'textarea',
                'class' => 'form-control',
                'rows'  => 7
            ),
            'options' => array(
                'label' => 'Description',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            )
        ));

        $this->add(array(
            'name' => 'expectedDate',
            'type' => 'Zend\Form\Element\Date',
            'attributes' => array(
                'class' => 'form-control',
                'required' => true
            ),
            'options' => array(
                'label' => 'Expected date',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            )
        ));

        $this->add(array(
            'name' => 'estimatedDate',
            'type' => 'Zend\Form\Element\Date',
            'attributes' => array(
                'class' => 'form-control',
                'required' => true
            ),
            'options' => array(
                'label' => 'Estimated date',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            )
        ));


//        $this->add(array(
//            'name' => 'origin_language',
//            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
//            'attributes' => array(
//                'class' => 'form-control'
//            ),
//            'options' => array(
//                'label' => 'Original Language',
//                'label_attributes' => array(
//                    'class' => 'control-label'
//                ),
//                'object_manager' => $this->getObjectManager(),
//                'target_class'   => 'Localit\Entity\Language',
//                'property'       => 'name',
//            )
//        ));

        $this->add([
            'name' => 'status',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => [
                'class' => 'form-control'
            ],
            'options' => [
                'label' => 'Task Status',
                'label_attributes' => array(
                    'class' => 'control-label'
                ),
                'value_options' => Entity\TaskAbstract::$allowedStatuses
            ]
        ]);

        $this->add(array(
            'name' => 'translate',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'attributes' => array(
                'class' => 'form-control',
                'multiple' => true,
                'data-localit-editable' => true,
                'data-localit-refbook'  => 'language'


            ),
            'options' => array(
                'label' => 'Translate to Languages',
                'label_attributes' => array(
                    'class' => 'control-label'
                ),
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Localit\Entity\Language',
                'property'       => 'name',
                'is_method'      => true,
                'find_method'    => array(
                    'name'   => 'findBy',
                    'params' => array(
                        'criteria' => ['trash' => ['$ne' => true]]
                    ),
                ),
            )
        ));


        $this->add(array(
            'name' => 'category',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'attributes' => array(
                'class' => 'form-control',
                'data-localit-editable' => true,
                'data-localit-refbook'  => 'category'
            ),
            'options' => array(
                'label' => 'Category',
                'label_attributes' => array(
                    'class' => 'control-label'
                ),
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Localit\Entity\Category',
                'property'       => 'name',
                'include_empty_option' => true,
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'empty_option_label' => '---',
                'is_method'      => true,
                'find_method'    => array(
                    'name'   => 'findBy',
                    'params' => array(
                        'criteria' => ['trash' => ['$ne' => true]]
                    ),
                ),

            )
        ));

        $this->add(array(
            'name' => 'comment',
            'attributes' => array(
                'type'  => 'textarea',
                'class' => 'form-control',
                'rows'  => 3
            ),
            'options' => array(
                'label' => 'Comment your changes',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            )
        ));

        $this->add(array(
            'name' => 'clone',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Save as copy',
                'class' => 'btn btn-default btn-info'
            ),
            'options' => array(
                'label' => 'Clone',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Confirm',
                'class' => 'btn btn-default btn-success'
            ),
            'options' => array(
                'label' => 'Confirm',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            )
        ));

        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type'  => 'submit',
                'formnovalidate' => '',
                'value' => 'discard',
                'class' => 'btn btn-default btn-danger'
            )
        ));


    }

    /**
     * Should return an array specification compatible with
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'name' => array(
                'required' => true
            ),
            'translate' => array(
                'required' => true
            ),
            'expectedDate' => array(
                'required' => true
            ),
            'estimatedDate' => array(
                'required' => false
            ),
            'status' => array(
                'required' => false
            ),
            'comment' => array(
                'required' => false
            ),
        );
    }

} 