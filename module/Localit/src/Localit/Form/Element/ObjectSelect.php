<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/1/13
 * Time: 3:19 PM
 */

namespace Localit\Form\Element;

use DoctrineModule\Form\Element\ObjectSelect as DoctrineObjectSelect;

class ObjectSelect extends DoctrineObjectSelect{

    /**
     * @return Proxy
     */
    public function getProxy()
    {
        if (null === $this->proxy) {
            $this->proxy = new Proxy();
        }
        return $this->proxy;
    }


} 