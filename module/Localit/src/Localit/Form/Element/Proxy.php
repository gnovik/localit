<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/1/13
 * Time: 3:21 PM
 */

namespace Localit\Form\Element;

use DoctrineModule\Form\Element\Proxy as DoctrineProxy;
use RuntimeException;

/**
 * Class Proxy
 * @package Localit\Form\Element
 */
class Proxy extends DoctrineProxy {

    protected $value_property;

    /**
     * @param mixed $value_property
     */
    public function setValueProperty($value_property)
    {
        $this->value_property = $value_property;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueProperty()
    {
        return $this->value_property;
    }

    public function setOptions($options)
    {
        parent::setOptions($options);

        if (isset($options['value_property'])) {
            $this->setValueProperty($options['value_property']);
        }

    }

    /**
     * Load value options
     *
     * @throws \RuntimeException
     * @return void
     */
    protected function loadValueOptions()
    {
        if (!($om = $this->objectManager)) {
            throw new RuntimeException('No object manager was set');
        }

        if (!($targetClass = $this->targetClass)) {
            throw new RuntimeException('No target class was set');
        }

        $metadata   = $om->getClassMetadata($targetClass);
        $identifier = $metadata->getIdentifierFieldNames();
        $objects    = $this->getObjects();
        $options    = array();

        if (empty($objects)) {
            $options[''] = '';
        } else {
            foreach ($objects as $key => $object) {
                if (($property = $this->property)) {
                    if ($this->isMethod == false && !$metadata->hasField($property)) {
                        throw new RuntimeException(sprintf(
                            'Property "%s" could not be found in object "%s"',
                            $property,
                            $targetClass
                        ));
                    }

                    $getter = 'get' . ucfirst($property);
                    if (!is_callable(array($object, $getter))) {
                        throw new RuntimeException(sprintf(
                            'Method "%s::%s" is not callable',
                            $this->targetClass,
                            $getter
                        ));
                    }

                    $label = $object->{$getter}();
                } else {
                    if (!is_callable(array($object, '__toString'))) {
                        throw new RuntimeException(sprintf(
                            '%s must have a "__toString()" method defined if you have not set a property or method to use.',
                            $targetClass
                        ));
                    }

                    $label = (string) $object;
                }

                if (($value_property = $this->value_property)) {

                    $getter = 'get' . ucfirst($value_property);
                    if (!is_callable(array($object, $getter))) {
                        throw new RuntimeException(sprintf(
                            'Method "%s::%s" is not callable',
                            $this->targetClass,
                            $getter
                        ));
                    }

                    $value = $object->{$getter}();
                }
                else    {
                    if (count($identifier) > 1) {
                        $value = $key;
                    } else {
                        $value = current($metadata->getIdentifierValues($object));
                    }
                }

                $options[] = array('label' => $label, 'value' => $value);
            }
        }

        $this->valueOptions = $options;
    }
} 