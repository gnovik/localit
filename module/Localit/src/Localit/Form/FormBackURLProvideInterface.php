<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/1/13
 * Time: 11:00 AM
 */

namespace Localit\Form;


interface FormBackURLProvideInterface {
    public function setBackURL($url);
    public function getBackURL();

} 