<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/1/13
 * Time: 11:00 AM
 */

namespace Localit\Form;


trait FormBackURLProvideTrait {

    public function setBackURL($url)    {
        if (!$this->has('backURL'))  {
            $this->add(array(
                'type' => 'hidden',
                'name' => 'backURL'
            ));
        }

        $this->get('backURL')->setValue($url);
    }

    public function getBackURL()    {
        if ($this->has('backURL'))  {
            return $this->get('backURL')->getValue();
        }
        else    {
            return null;
        }
    }
} 