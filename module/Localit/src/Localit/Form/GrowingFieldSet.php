<?php

namespace Localit\Form;

use Zend\Form;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/5/13
 * Time: 5:40 PM
 */
class GrowingFieldSet extends Form\Fieldset {

    protected $inputFilterSpecification = [];


    public function setInputFilterSpecification($spec)   {
        $this->inputFilterSpecification = $spec;
    }

    public function getInputFilterSpecification()
    {
        return $this->inputFilterSpecification;
    }
}