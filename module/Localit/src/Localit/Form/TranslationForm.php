<?php

namespace Localit\Form;

use Zend\Form\Element;
use Zend\Form;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Persistence\ProvidesObjectManager;

use Doctrine\Common\Persistence\ObjectManager;
use Zend\InputFilter\InputFilterProviderInterface;

use Zend\Form\Exception;

use \Localit\Entity;

use Zend\Stdlib\ArrayUtils;

use Traversable;

/**
 * Class CreateTask
 */
class TranslationForm extends Form\Form implements InputFilterProviderInterface {

    protected $inputFilterSpec = [];

    public function __construct($formName = 'translation_form')   {
        parent::__construct($formName);

        $this->setHydrator(new ClassMethodsHydrator());

        $this->add(array(
            'type' => 'hidden',
            'name' => 'id'
        ));

        $this->add(array(
            'name' => 'approve',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'approve',
                'class' => 'btn btn-default btn-success lower'
            )
        ));

        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'discard',
                'class' => 'btn btn-default btn-danger lower'
            )
        ));


    }

    public function initialize($chunks, $fieldSetName = 'chunks', $isRequired = false)    {
        $factory = $this->getFormFactory();

        $chunksFieldSet = new GrowingFieldSet($fieldSetName);
        $this->add($chunksFieldSet);

        $this->inputFilterSpec[$fieldSetName] = [
            'type' => 'Zend\InputFilter\InputFilter'
        ];

        $elementIndex = 0;
        foreach ($chunks as $chunk) {

            $chunkFieldSet = new GrowingFieldSet($elementIndex);

            $this->inputFilterSpec[$fieldSetName][$elementIndex] = [
                'type' => 'Zend\InputFilter\InputFilter'
            ];


            foreach ($chunk as $section => $value)  {
                $this->inputFilterSpec[$fieldSetName][$elementIndex][$section] = ['required' => $isRequired];

                $element = $factory->create(array(
                    'name' => "$section",
                    'attributes' => array(
                        'type'    => 'textarea',
                        'class'   => "form-control localit-$section",
                        'required' => $isRequired
                    )
                ));
                $element->setValue($value);

                $chunkFieldSet->add($element);
            }


            $chunksFieldSet->add($chunkFieldSet);

            $elementIndex++;
        }

    }


    /**
     * Should return an array specification compatible with
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return $this->inputFilterSpec;
    }

} 