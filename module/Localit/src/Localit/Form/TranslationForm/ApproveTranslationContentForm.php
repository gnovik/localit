<?php

namespace Localit\Form\TranslationForm;

use Zend\Form\Element;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Persistence\ProvidesObjectManager;

use Doctrine\Common\Persistence\ObjectManager;
use Zend\InputFilter\InputFilterProviderInterface;

use Zend\Form\Exception;

use \Localit\Entity;
use \Localit\Form;

use Zend\Stdlib\ArrayUtils;

use Traversable;

/**
 * Class CreateTask
 */
class ApproveTranslationContentForm extends Form\TranslationForm implements InputFilterProviderInterface {

    public function __construct()   {
        parent::__construct('edit_task_content');

        $this->add([
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'includeExtraFields',
            'options' => [
                'label' => 'These fields doesn\'t appear in original file, do you want Include These Extra Fields?'
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'language'
        ]);

        $this->inputFilterSpec['includeExtraFields'] = [
            'required' => false
        ];
    }


}