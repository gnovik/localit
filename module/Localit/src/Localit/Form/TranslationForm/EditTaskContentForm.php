<?php

namespace Localit\Form\TranslationForm;

use Zend\Form\Element;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Persistence\ProvidesObjectManager;

use Doctrine\Common\Persistence\ObjectManager;
use Zend\InputFilter\InputFilterProviderInterface;

use Zend\Form\Exception;

use \Localit\Entity;
use \Localit\Form;

use Zend\Stdlib\ArrayUtils;

use Traversable;
use Zend\Form\FormInterface;

/**
 * Class CreateTask
 */
class EditTaskContentForm extends Form\TranslationForm {

    public function __construct()   {
        parent::__construct('edit_task_content');
    }

    public function bind($object, $flags = FormInterface::VALUES_NORMALIZED)   {
        if ($object instanceof Entity\TaskAbstract)    {
            $this->initialize($object->getChunks());

            parent::bind($object, $flags);
        }
        else    {
            throw new \Exception('Object should be an instance of \Localit\Entity\TaskAbstract');
        }
    }

}