<?php

namespace Localit\Form;

use Zend\Form\Element;
use Zend\Form;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class UploadTaskFile
 */
class UploadTaskFile extends Form\Form implements InputFilterProviderInterface {

    public function __construct()   {
        parent::__construct('uplpoad_task_file');

        $this->setHydrator(new ClassMethodsHydrator());

        $this->add(array(
            'name' => 'taskFile',
            'type' => 'Zend\Form\Element\File',
            'attributes' => array(
                'class'    => 'form-element',
                'required' => true
            ),
            'options' => array(
                'label' => 'Upload Task File',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            )

        ));

        $this->add([
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'asFile',
            'options' => [
                'label' => "Don't parse this file (Will be stored in database originally)"
            ]
        ]);



        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'value' => 'upload',
                'class' => 'btn btn-default lower'
            )

        ));

    }

    /**
     * Should return an array specification compatible with
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'taskFile' => array(
                'required' => true
            )
        );
    }

}