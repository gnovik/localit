<?php

namespace Localit\Form;

use Zend\Form\Element;
use Zend\Form;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class UploadTranslation
 */
class UploadTranslation extends Form\Form implements InputFilterProviderInterface {

    public function __construct()   {
        parent::__construct('upload_translation');

        $this->setHydrator(new ClassMethodsHydrator());

        $this->add(array(
            'type' => 'hidden',
            'name' => 'language'
        ));

        $this->add(array(
            'name' => 'file',
            'type' => 'Zend\Form\Element\File',
            'attributes' => array(
                'class'    => 'form-element',
                'required' => true
            ),
            'options' => array(
                'label' => 'Upload Translation File',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            )

        ));


        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'value' => 'upload',
                'class' => 'btn btn-default lower'
            )

        ));

    }

    /**
     * Should return an array specification compatible with
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'file' => array(
                'required' => true
            )
        );
    }

}