<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/15/13
 * Time: 4:10 PM
 */

namespace Localit\Listener;

use Doctrine\MongoDB\IteratorAggregate;
use Zend\Paginator\Adapter\Iterator;
use Zend\ServiceManager;
use Doctrine\ODM\MongoDB\Event;
use Localit\Entity;

class DoctrineEvents implements ServiceManager\ServiceLocatorAwareInterface {
    use ServiceManager\ServiceLocatorAwareTrait;

    public function preUpdate(Event\PreUpdateEventArgs $eventArgs)    {
        $document = $eventArgs->getDocument();

        if ($document instanceof Entity\ActivityLogEntityInterface)    {
            $changeSet = $eventArgs->getDocumentChangeSet();

            $logConfig = $document->getLogConfig();
            $logFields = array_key_exists('fields', $logConfig) ? $logConfig['fields'] : $logConfig;
            $changeSet = array_intersect_key($changeSet, $logFields);

            if (empty($changeSet)) return;

            foreach ($changeSet as $field => &$change) {
                if (!$logFields[$field])    {
                    unset($changeSet[$field]);
                    continue;
                }

                $change = [
                    'before' => $change[0],
                    'after'  => $change[1],
                    'beforeType' => gettype($change[0]) . (is_object($change[0]) ? get_class($change[0]) : ''),
                    'afterType' => gettype($change[1]) . (is_object($change[1]) ? get_class($change[1]) : '')
                ];

                foreach ($change as &$part)  {
                    if (is_object($part))  {
                        if (method_exists($part, '__toString')) {
                            $part = $part->__toString();
                        }
                        else    {
                            switch (true)   {
                                case $part instanceof \Traversable:
                                    $part = iterator_to_array($part, true);
                                    break;

                                case $part instanceof \DateTime:
                                    $part = $part->format('Y-m-d H:i:s');
                                    break;
                            }
                        }

                    }
                }
            }

            $logEntry = new Entity\LogEntry();

            if (array_key_exists('commentField', $logConfig)) {
                $commentField = $logConfig['commentField'];
                $commentFieldGetter = 'get' . ucfirst($commentField);
                $commentFieldSetter = 'set' . ucfirst($commentField);
                $logEntry->setComment($document->$commentFieldGetter());

                $document->$commentFieldSetter(null);
            }

            if (!empty($changeSet))  {
                $logEntry->setChangeSet($changeSet);
                $logEntry->setType(Entity\LogEntry::TYPE_PATCH);
            }
            else    {
                if ($logEntry->getComment()) {
                    $logEntry->setType(Entity\LogEntry::TYPE_COMMENT);
                }
                else    {
                    //no sense to proceed
                    return;
                }
            }



            $user = $this->getUser();
            if ($user)  {
                $logEntry->setUser($user);
            }

            $document->appendLog($logEntry);

            $this->recomputeDocumentChangeset($eventArgs);
        }
    }

    public function prePersist(Event\LifecycleEventArgs $eventArgs)    {
        $document = $eventArgs->getDocument();

        if ($document instanceof Entity\ActivityLogEntityInterface)    {

            $logEntry = new Entity\LogEntry();
            $logEntry->setType(Entity\LogEntry::TYPE_CREATE);

            $user = $this->getUser();
            if ($user)  {
                $logEntry->setUser($user);
            }

            $document->appendLog($logEntry);

        }
    }

    protected function recomputeDocumentChangeset($eventArgs) {
        $document = $eventArgs->getDocument();
        $dm = $eventArgs->getDocumentManager();
        $class = $dm->getClassMetadata(get_class($document));
        $dm->getUnitOfWork()->recomputeSingleDocumentChangeSet($class, $document);
    }

    protected function getUser()    {
        try {
            $authService = $this->getServiceLocator()->get('zfcuser_auth_service');
        }
        catch(\Exception $e)    {
            $authService = null;
        }

        if ($authService)   {
            $user = $authService->getIdentity();
        }
        else    {
            $user = null;
        }

        return $user;
    }

} 