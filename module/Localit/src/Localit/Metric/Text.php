<?php

namespace Localit\Metric;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/13/13
 * Time: 4:02 PM
 */

class Text {


    public static function getWordsCount($text) {
        return str_word_count($text, 0, 'АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя');
    }
} 