<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 12/2/13
 * Time: 3:57 PM
 */

namespace Localit\Notification;

use Localit\Controller\Traits\FetchDocumentManagerTrait;
use Zend\EventManager\Event,
    Zend\EventManager\EventManagerInterface,
    Zend\EventManager\AbstractListenerAggregate,
    Zend\Mail;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

use Localit\Entity;



class NotificationManager extends AbstractListenerAggregate implements ServiceLocatorAwareInterface {
    use ServiceLocatorAwareTrait;
    use FetchDocumentManagerTrait;

    protected $usersNotificationMap = [
        'translator' => ['createTask']
    ];

    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('create_task', array($this, 'createTask'));
        $this->listeners[] = $events->attach('upload_translation', array($this, 'uploadTranslation'));
    }


    public function createTask(Event $e)    {
        $task = $e->getParam('task', false);
        $user = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity();

        $usersRepo = $this->getServiceLocator()->get('Localit\UserRepository');
        $translators = $usersRepo->getUsersByRole('translator');

        $subscribers = $usersRepo->fetchUsersEmails($translators);

        $di = $this->getServiceLocator()->get('Localit\MailNotificationDI');

        $viewModel = $di->get('Zend\View\Model\ViewModel');

        $viewModel->setTemplate('localit/notification/create-task-translator');
        $viewModel->setVariable('task', $task);
        $viewModel->setVariable('user', $user);

        $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');

        $mailBody = $renderer->render($viewModel);


        $notification = new Entity\Notification();
        $notification->setSubject('New Task Notification');
        $notification->setFrom($user->getEmail());
        $notification->setSubscribers($subscribers);
        $notification->setBody($mailBody);

        $this->getDM()->persist($notification);
        $this->getDM()->flush();

    }

    public function uploadTranslation(Event $e) {
        $task = $e->getParam('task', false);
        $language = $e->getParam('language', false);
        $user = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity();

        $usersRepo = $this->getServiceLocator()->get('Localit\UserRepository');
        $users = $usersRepo->getUsersByRole('manager');

        $subscribers = $usersRepo->fetchUsersEmails($users);

        $viewModel = new ViewModel();

        $viewModel->setTemplate('localit/notification/manager/upload-translation');
        $viewModel->setVariable('task', $task);
        $viewModel->setVariable('user', $user);
        $viewModel->setVariable('language', $language);

        $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');

        $mailBody = $renderer->render($viewModel);


        $notification = new Entity\Notification();
        $notification->setSubject('New Task Notification');
        $notification->setFrom($user->getEmail());
        $notification->setSubscribers($subscribers);
        $notification->setBody($mailBody);

        $this->getDM()->persist($notification);
        $this->getDM()->flush();
    }


} 