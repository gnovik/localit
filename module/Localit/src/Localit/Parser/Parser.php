<?php
namespace Localit\Parser;
use Doctrine\MongoDB\Iterator;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/4/13
 * Time: 11:01 AM
 */
class Parser implements \Iterator {

    protected $parsed = array();

    protected $source;

    protected $options = [];

    public function __construct($source, $options = [])   {
        $this->source = $source;
        $this->options = $options;
    }


    public function getParsedData() {
        return $this->parsed;
    }

    public function rewind() {
        return reset($this->parsed);
    }

    public function key()   {
        return key($this->parsed);
    }

    public function current()   {
        return current($this->parsed);
    }

    public function next()  {
        return next($this->parsed);
    }

    public function prev()  {
        return prev($this->parsed);
    }

    public function count()  {
        return count($this->parsed);
    }

    public function valid()  {
        return valid($this->parsed);
    }


    public static function getParserForFile($path, $originFileName = '', $mimeType = false, $options = [])  {
        if ($mimeType === false)    {
            $fInfo = new finfo(FILEINFO_MIME, "/usr/share/misc/magic");

            $mimeType = $fInfo->file($path);
        }

        switch($mimeType)    {
            case 'application/msword':
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                return false;

                break;

            case 'application/vnd.ms-excel':
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                $parserClass = 'Localit\Parser\Xls';

                break;

            case 'text/csv':
                return false;

            default:
                $ext = substr($originFileName, strrpos($originFileName, '.') + 1);

                switch($ext)    {
                    case 'doc':
                    case 'docx':
                    return false;

                        break;

                    case 'xls':
                    case 'xlsx':
                        $parserClass = 'Localit\Parser\Xls';

                        break;

                    default:
                        return false;
                }
        }

        return new $parserClass($path, $options);
    }

    public function isParseable()   {
        return true;
    }

} 