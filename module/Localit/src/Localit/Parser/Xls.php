<?php
namespace Localit\Parser;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/4/13
 * Time: 11:01 AM
 */
class Xls extends Parser {

    public function parse($readTranslatedColumn = true, $removeDuplicates = true) {

        $reader = new \PHPExcel_Reader_Excel2007();
        $excel = $reader->load($this->source);

        $sheet = $excel->getSheet(0);

        $translateLanguage = array_key_exists('translateLanguage', $this->options) ?
            $this->options['translateLanguage'] : 'translated';

//echo '<pre>';
//        var_dump($removeDuplicates);
//        var_dump($readTranslatedColumn);
        $originValuesStack = array();
        foreach ($sheet->getRowIterator() as $row)  {
            $rowIndex = $row->getRowIndex();

            $cellOrigin = $sheet->getCellByColumnAndRow(0, $rowIndex);
            $originValue = trim((string)$cellOrigin->getValue());


            if (strlen($originValue) > 0 && !in_array($originValue, $originValuesStack))  { //remove blank lines
                if ($removeDuplicates)  { //save stack only if removeDuplicates enabled
                    $originValuesStack[] = $originValue;
                }

                $this->parsed[$rowIndex]['origin'] = $originValue;

                if ($readTranslatedColumn)  {
                    $cellTranslated = $sheet->getCellByColumnAndRow(1, $rowIndex);
//                    echo '---';
//                    var_dump($originValue); echo PHP_EOL;
//                    var_dump($cellTranslated->getValue());echo PHP_EOL;
//                    echo '---';

                    $this->parsed[$rowIndex][$translateLanguage] = trim((string)$cellTranslated->getValue());
                }
            }
        }
//        var_dump($this->parsed);
//        echo '</pre>';exit();
        return true;
    }

    public function fetchTextData($readTranslatedColumn = true) {
        $this->parse($readTranslatedColumn);
        $data = '';
        foreach ($this->parsed as $row) {
            $data .= $row['origin'] . PHP_EOL;
        }

        return $data;
    }


} 