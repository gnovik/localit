<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/26/13
 * Time: 9:43 AM
 */

namespace Localit\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Localit\Entity;

class FileOutput extends AbstractPlugin  {

    protected $file;

    protected $options = [];

    public function __invoke(Entity\File $file, $options = [])  {
        $this->file = $file;
        $this->file->getFile();
        $this->options = $options;


        try {
            $mime = $this->file->getMime();

            $fileName = array_key_exists('fileName', $this->options) ? $this->options['fileName'] : $this->file->getOriginName();

            $response = $this->getController()->getResponse();
            $response->getHeaders()->addHeaders(array(
                'Content-Type' => $mime,
                'Content-Disposition' => "attachment;filename=\"{$fileName}\"",
                'Cache-Control' => 'max-age=0',
            ));
            $response->setContent($this->file->getFile()->getBytes());

            return $response;
        }
        catch(\Exception $e)    {
            echo '<pre>' . (string)$e . '</pre>';
            exit();
        }
    }

}