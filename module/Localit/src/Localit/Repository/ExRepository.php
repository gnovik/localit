<?php

namespace Localit\Repository;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/12/13
 * Time: 10:46 AM
 */

use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ODM\MongoDB\LockMode;
use Localit\Entity;


class ExRepository extends DocumentRepository implements ProvideInstanceCounterInterface
{
    /**
     * @return int
     */
    public function generateNextInstanceNumber()  {
//        $doc = $this->findOneBy(['type' => 'service']);
        $doc = $this->uow->getDocumentPersister('\Localit\Entity\InstanceIndex')->load(['type' => 'service']);

        if ($doc)   {
            $number = $doc->getInstanceCounter() + 1;
        }
        else    {

//            $this->getClassMetadata()->getReflectionClass()->getInterfaceNames()

            $doc = new Entity\InstanceIndex();
            $number = 1;
        }

        $doc->setInstanceCounter($number);


        $this->dm->persist($doc);

        return (string)$number;
    }

    /**
     * @param int $number
     *
     * @return object
     */
    public function getByNumber($number)    {
        return $this->findOneBy(['instanceNumber' => (string)$number]);
    }
}