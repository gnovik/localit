<?php

namespace Localit\Repository;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/12/13
 * Time: 10:46 AM
 */

use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ODM\MongoDB\LockMode;

class OverIdRepository extends DocumentRepository
{
    public function find($id, $lockMode = LockMode::NONE, $lockVersion = null)  {

        $mongoId = new \MongoId($id);
        if ((string)$mongoId === $id)    {
            return parent::find($id, $lockMode, $lockVersion);
        }
        else    {
            foreach ($this->class->getReflectionProperties() as $property) {
                $comment = $property->getDocComment();
                if (strpos($comment, '@OverId') !== false)    {
                    return $this->findOneBy([
                        $property->getName() => $id
                    ]);
                }
            }

            return null;
        }
    }
}