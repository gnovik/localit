<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/15/13
 * Time: 2:19 PM
 */

namespace Localit\Repository;


interface ProvideInstanceCounterInterface {

    public function generateNextInstanceNumber();

} 