<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/1/13
 * Time: 5:55 PM
 */

namespace Localit\View\Helper;

use Zend\View\Helper\AbstractHelper;

class BootstrapElement extends AbstractHelper {

    public function __invoke($formHelper, $element, $options = array())  {
        $groupClass = 'form-group';

        if ($element->getMessages())    {
            $groupClass .= ' has-error';
        }




        return $this->getView()->render('localit/forms/element/bootstrap-element', array(
            'groupClass' => $groupClass,
            'element'    => $element,
            'formHelper' => $formHelper,
            'options'    => $options
        ));

    }
} 