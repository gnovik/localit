<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/18/13
 * Time: 11:58 AM
 */

namespace Localit\View\Helper;

use Zend\View\Helper\AbstractHelper;

class LogRenderer extends AbstractHelper {

    public function __invoke($log)  {
        return $this->getView()->render('localit/log/render-short', [
            'log' => $log
        ]);
    }
} 