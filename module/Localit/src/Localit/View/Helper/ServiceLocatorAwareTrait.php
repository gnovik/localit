<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/11/13
 * Time: 11:29 AM
 */

namespace Localit\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareTrait as ZendServiceLocatorAwareTrait;
use Zend\View\HelperPluginManager;

trait ServiceLocatorAwareTrait {
    use ZendServiceLocatorAwareTrait;

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        if ($this->serviceLocator instanceof HelperPluginManager)   {
            return $this->serviceLocator->getServiceLocator();
        }
        else    {
            return $this->serviceLocator;
        }

    }

    public function getHelperManager()  {
        return $this->serviceLocator;
    }
}

