<?php

namespace Localit\View\Helper;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 10/31/13
 * Time: 12:07 PM
 */

use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Doctrine\ODM\MongoDB\DocumentManager;

use \Localit\Entity;

class ShowAddForm extends AbstractHelper implements ServiceLocatorAwareInterface {
    use ServiceLocatorAwareTrait;

    public function __invoke($step = 1, $options = array())  {
        $helperManager = $this->getServiceLocator();
        $locator = $helperManager->getServiceLocator();

        switch($step)   {
            case 1:
                $form = $locator->get('localitUploadTaskFileForm');

                return $this->getView()->render('localit/forms/upload-task-file', array(
                    'form' => $form,
                    'options' => $options
                ));

            case 2:

                $form = $locator->get('localitCreateTaskForm');

                if (array_key_exists('task', $options) && $options['task']) {
                    $task = $options['task'];

                    if (is_scalar($task)) {
                        $task = $locator->get('TaskRepository')->find($task);
                    }

                    $form->get('id')->setValue($task->getId());
                }


                return $this->getView()->render('localit/forms/show-add-form', array(
                    'form' => $form,
                    'task' => $task
                ));

            default:
                throw new Exception("Step `$step` is not allowed");
        }



    }
} 