<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/11/13
 * Time: 11:21 AM
 */

namespace Localit\View\Helper;



use Zend\View\Helper\AbstractHelper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;

use \Localit\Entity;

use Localit\Controller\Traits\FetchDocumentManagerTrait;

class ShowEditForm extends AbstractHelper implements ServiceLocatorAwareInterface {
    use ServiceLocatorAwareTrait;
    use FetchDocumentManagerTrait;

    public function __invoke($task, $options = array())  {
        $locator = $this->getServiceLocator();

        $form = $locator->get('localitCreateTaskForm');

        if ($task) {
            if (is_scalar($task))   {
                $rep = $locator->get('TaskRepository');
                $task = $rep->find($task);
            }
        }
        else    {
            throw new \Exception("Task should be specified. `$task` given");
        }

        $form->bind($task);

        return $this->getView()->render('localit/forms/show-edit-form', array(
            'form' => $form,
            'options' => $options,
            'task' => $task
        ));

    }

} 