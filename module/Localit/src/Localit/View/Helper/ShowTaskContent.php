<?php

namespace Localit\View\Helper;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 10/31/13
 * Time: 12:07 PM
 */

use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Doctrine\ODM\MongoDB\DocumentManager;

use DoctrineModule\Persistence\ProvidesObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;

use Localit\Entity;

class ShowTaskContent extends AbstractHelper implements ObjectManagerAwareInterface, ServiceLocatorAwareInterface {
    use ServiceLocatorAwareTrait, ProvidesObjectManager;

    public function __invoke($task, $options = array())  {

        $helperManager = $this->getServiceLocator();
        $locator = $helperManager->getServiceLocator();

        $dm = $this->getObjectManager();

        if (is_scalar($task))   {
            $task = $dm->getRepository('\Localit\Entity\TaskAbstract')->find($task);
        }

        if (!($task instanceof Entity\TaskAbstract))    {
            throw new \Exception('Entity not given to view helper');
        }

        $form = $locator->get('localiteEditTaskContentForm');
        $form->initialize($task->getChunks());

        $form->get('id')->setValue($task->getId());

        return $this->getView()->render('localit/task-content/editor', array(
            'task' => $task,
            'form' => $form,
            'options' => $options
        ));

    }
}