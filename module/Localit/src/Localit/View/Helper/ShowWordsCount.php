<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/1/13
 * Time: 5:55 PM
 */

namespace Localit\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Localit\Entity\TaskFile;

class ShowWordsCount extends AbstractHelper {

    public function __invoke($task, $options = [])  {

        if ($task instanceof TaskFile)   {
            $data = [
                'unique' => $task->getQuantityWordsUnique(),
                'nonUnique' => $task->getQuantityWordsNonUnique()
            ];

            if (array_key_exists('short', $options) && $options['short'])    {
                return $data['unique'] . '/' . $data['nonUnique'];
            }
            else    {
                return $this->getView()->render('localit/task-content/words-count-taskfile', $data);
            }

        }
        else    {
            return $this->getView()->render('localit/task-content/words-count-taskinstant', array(
                'wordsCount' => $task->getWordsCount()
            ));
        }



    }
} 