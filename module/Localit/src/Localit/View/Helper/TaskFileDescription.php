<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 11/1/13
 * Time: 5:55 PM
 */

namespace Localit\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Localit\Entity\TaskFile;

class TaskFileDescription extends AbstractHelper {

    public function __invoke($task)  {

        if ($task instanceof TaskFile)   {
            $type = 'file';
            $fileExtension = $task->getOriginFile()->getFileExtension();
        }
        else    {
            $type = 'instant';
            $fileExtension = 'xlsx';
        }

        return $this->getView()->render('localit/task-content/task-file-description', array(
            'type' => $type,
            'extension' => $fileExtension
        ));

    }
} 