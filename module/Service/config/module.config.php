<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Service\Controller\Notification' => 'Service\Controller\NotificationController',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
                'index' => array(
                    'options' => array(
                        'route'    => 'notification',
                        'defaults' => array(
                            'controller' => 'Service\Controller\Notification',
                            'action'     => 'process',
                        ),
                    ),
                ),
            ),
        )
    )
);
