<?php

namespace Service\Controller;

use Doctrine\Tests\Common\Annotations\Ticket\Doctrine\ORM\Mapping\Entity;
use Localit\Controller\Traits\FetchDocumentManagerTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ConsoleModel;
use Zend\Console\Adapter as Console;
use Zend\Console\Console as ConsoleStatic;
use Zend\Console\Adapter\Virtual;
use Zend\Console\ColorInterface as Color;

use Doctrine\Common\ClassLoader,
    Doctrine\Common\Annotations\AnnotationReader,
    Doctrine\ODM\MongoDB\DocumentManager,
    Doctrine\MongoDB\Connection,
    Doctrine\ODM\MongoDB\Configuration,
    Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;


use Localit\Entity\Notification;
use Localit\Entity\LogEntry;

use Zend\Debug\Debug;

use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

/**
 * User: fliak
 * Date: 2/28/13
 * Time: 9:42 PM
 */
class NotificationController extends AbstractActionController  {

    use FetchDocumentManagerTrait;

    public function processAction()   {
        $blockFile = '/tmp/localit_notification_process';
        if (file_exists($blockFile))    {
            exit(1);
        }
        file_put_contents($blockFile, date('Y-m-d H:i:s'));

        $repo = $this->getDM()->getRepository('Localit\Entity\Notification');
        $notifications = $repo->findBy([
            'status' => Notification::NOTIFICATION_STATUS_WAITING
        ]);

        $mailDI = $this->getServiceLocator()->get('Localit\MailNotificationDI');
        $transport = $mailDI->get('Zend\Mail\Transport\Sendmail');
        $message = $mailDI->get('Zend\Mail\Message');


        if ($notifications->count() === 0)  {
            echo 'No messages in queue' . PHP_EOL;
        }

        foreach ($notifications as $notification)   {
            try {
                echo "Process notification " . $notification->getId() . PHP_EOL;

                $message = new Message();


                $message->setFrom($notification->getFrom());

                foreach ($notification->getSubscribers() as $email) {
                    $message->addTo($email);
                    Debug::dump($email, 'addto');
                }


                $message->setSubject($notification->getSubject());
                $message->setEncoding("UTF-8");

                $html = new MimePart($notification->getBody());
                $html->type = "text/html";

                $body = new MimeMessage();
                $body->setParts([$html]);

                $message->setBody($body);

                $notification->setStatus(Notification::NOTIFICATION_STATUS_TRYSEND);

                $this->getDM()->persist($notification);

                $transport->send($message);

                $notification->setStatus(Notification::NOTIFICATION_STATUS_SENT);
            }
            catch (\Exception $e)    {
                $logEntry = new LogEntry();
                $logEntry->setComment($e->getMessage());
                $logEntry->setType(LogEntry::TYPE_PROCESS);
                $notification->appendLog($logEntry);
                $notification->setStatus(Notification::NOTIFICATION_STATUS_ERROR);
            }
        }

        try {
            $this->getDM()->flush();
        }
        catch(\Exception $e)    {
            unlink($blockFile);

            echo 'Save-time exception. Check server.' . PHP_EOL;

            throw $e;
        }

        unlink($blockFile);

        return 'finish' . PHP_EOL;
    }

}