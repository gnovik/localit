<?php

//if ($_SERVER['APPLICATION_ENV'] == 'development') {
/**
 * @FIXME SETUP ENV VARS AND MOVE ini_set to php.ini
 */

error_reporting(E_ALL);
    ini_set("display_errors", 1);

    ini_set('session.use_cookies', 1);
    ini_set('session.cookie_lifetime', 0);
    ini_set('session.cookie_path', '/');
    ini_set('session.cookie_domain', '');
    ini_set('session.cookie_secure', '');
    ini_set('session.cookie_httponly', '');

    ini_set('session.name', 'LOCALITSESSIONID');

    $sessionPath = realpath(__DIR__ . '/../data/session');
    if ($sessionPath)   {
        ini_set('session.save_path', $sessionPath);
    }

//}

umask(017);

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

// Setup autoloading
require 'init_autoloader.php';

$config = require 'config/application.config.php';

switch(php_sapi_name()) {
    case 'cli':
        if (array_key_exists('cli_modules', $config))   {
            $config['modules'] = $config['cli_modules'];
        }
        break;
}




// Run the application!
Zend\Mvc\Application::init($config)->run();
