var DateRange = function(baseDate)  {
    baseDate = baseDate || new Date();
    this.baseDate = baseDate;
    this.lBound = new Date(baseDate.valueOf());
    this.uBound = new Date(baseDate.valueOf());
}
DateRange.prototype = {
    setMonthRange: function(paramLeft, paramRight) {
        this.lBound.setDate(1);
        this.lBound.setMonth(this.lBound.getMonth() + paramLeft);

        this.uBound.setDate(1);
        this.uBound.setMonth(this.uBound.getMonth() + paramRight);
        this.uBound.setDate(0);
    }
};