var Iterator = function(iterableObject)    {
    this.arr = iterableObject;
    this.reset();
};

Iterator.prototype = {
    currentKey: undefined,
    currentIndex: undefined,

    getMinMax: function()   {
        var i, val, min, max;
        min = max = this.current();
        for (i in this.arr) {
            if (val = this.arr[i] < min) {
                min = this.arr[i];
            }
            if (val = this.arr[i] > max) {
                max = this.arr[i];
            }
        }

        return {
            min: Number(min),
            max: Number(max)
        };
    },
    key: function() {
        return this.currentKey;
    },
    index: function()   {
        return this.currentIndex;
    },
    current: function() {
        return this.arr[this.currentKey];
    },
    reset: function()   {
        this.currentIndex = 0;
        for (var i in this.arr) {
            this.currentKey = i;

            return this.arr[i];
        }

    }
};