/**
 * Created by fliak on 11/5/13.
 */

if (typeof(jQuery) !== 'undefined') {
    jQuery.prototype.enable = function()    {
        $(this).find('[disabled]').removeAttr('disabled');
    }

    jQuery.prototype.disable = function()    {
        $(this).find('fieldset').attr('disabled', 'disabled');
    }

}
