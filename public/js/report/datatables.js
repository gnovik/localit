var dataTableObject;

/* Set the defaults for DataTables initialisation */
$.extend( true, $.fn.dataTable.defaults, {
	"sDom": "<''<''l><''f>r>t<''<''i><''p>>",
	"sPaginationType": "bootstrap",
	"oLanguage": {
		"sLengthMenu": "_MENU_ records per page"
	}
} );


/* Default class modification */
$.extend( $.fn.dataTableExt.oStdClasses, {
	"sWrapper": "dataTables_wrapper form-inline"
} );


/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
	return {
		"iStart":         oSettings._iDisplayStart,
		"iEnd":           oSettings.fnDisplayEnd(),
		"iLength":        oSettings._iDisplayLength,
		"iTotal":         oSettings.fnRecordsTotal(),
		"iFilteredTotal": oSettings.fnRecordsDisplay(),
		"iPage":          oSettings._iDisplayLength === -1 ?
			0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
		"iTotalPages":    oSettings._iDisplayLength === -1 ?
			0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
	};
};/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
	return {
		"iStart":         oSettings._iDisplayStart,
		"iEnd":           oSettings.fnDisplayEnd(),
		"iLength":        oSettings._iDisplayLength,
		"iTotal":         oSettings.fnRecordsTotal(),
		"iFilteredTotal": oSettings.fnRecordsDisplay(),
		"iPage":          oSettings._iDisplayLength === -1 ?
			0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
		"iTotalPages":    oSettings._iDisplayLength === -1 ?
			0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
	};
};

/*
 * Function: fnGetColumnData
 * Purpose:  Return an array of table values from a particular column.
 * Returns:  array string: 1d data array
 * Inputs:   object:oSettings - dataTable settings object. This is always the last argument past to the function
 *           int:iColumn - the id of the column to extract the data from
 *           bool:bUnique - optional - if set to false duplicated values are not filtered out
 *           bool:bFiltered - optional - if set to false all the table data is used (not only the filtered)
 *           bool:bIgnoreEmpty - optional - if set to false empty values are not filtered from the result array
 * Author:   Benedikt Forchhammer <b.forchhammer /AT\ mind2.de>
 */
$.fn.dataTableExt.oApi.fnGetColumnData = function ( oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty ) {
    // check that we have a column id
    if ( typeof iColumn == "undefined" ) return new Array();

    // by default we only want unique data
    if ( typeof bUnique == "undefined" ) bUnique = true;

    // by default we do want to only look at filtered data
    if ( typeof bFiltered == "undefined" ) bFiltered = true;

    // by default we do not want to include empty values
    if ( typeof bIgnoreEmpty == "undefined" ) bIgnoreEmpty = true;

    // list of rows which we're going to loop through
    var aiRows;

    // use only filtered rows
    if (bFiltered == true) aiRows = oSettings.aiDisplay;
    // use all rows
    else aiRows = oSettings.aiDisplayMaster; // all row numbers

    // set up data array
    var asResultData = new Array();

    for (var i=0,c=aiRows.length; i<c; i++) {
        iRow = aiRows[i];
        var aData = this.fnGetData(iRow);
        var sValue = aData[iColumn];

        // ignore empty values?
        if (bIgnoreEmpty == true && sValue.length == 0) continue;

        // ignore unique values?
        else if (bUnique == true && jQuery.inArray(sValue, asResultData) > -1) continue;

        // else push the value onto the result data array
        else asResultData.push(sValue);
    }

    return asResultData;
};

/* Bootstrap style pagination control */
$.extend( $.fn.dataTableExt.oPagination, {
	"bootstrap": {
		"fnInit": function( oSettings, nPaging, fnDraw ) {
			var oLang = oSettings.oLanguage.oPaginate;
			var fnClickHandler = function ( e ) {
				e.preventDefault();
				if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
					fnDraw( oSettings );
				}
			};

			$(nPaging).addClass('pagination').append(
				'<ul class="pagination">'+
					'<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
					'<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
				'</ul>'
			);
			var els = $('a', nPaging);
			$(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
			$(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
		},

		"fnUpdate": function ( oSettings, fnDraw ) {
			var iListLength = 5;
			var oPaging = oSettings.oInstance.fnPagingInfo();
			var an = oSettings.aanFeatures.p;
			var i, ien, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

			if ( oPaging.iTotalPages < iListLength) {
				iStart = 1;
				iEnd = oPaging.iTotalPages;
			}
			else if ( oPaging.iPage <= iHalf ) {
				iStart = 1;
				iEnd = iListLength;
			} else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
				iStart = oPaging.iTotalPages - iListLength + 1;
				iEnd = oPaging.iTotalPages;
			} else {
				iStart = oPaging.iPage - iHalf + 1;
				iEnd = iStart + iListLength - 1;
			}

			for ( i=0, ien=an.length ; i<ien ; i++ ) {
				// Remove the middle elements
				$('li:gt(0)', an[i]).filter(':not(:last)').remove();

				// Add the new list items and their event handlers
				for ( j=iStart ; j<=iEnd ; j++ ) {
					sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
					$('<li '+sClass+'><a href="#">'+j+'</a></li>')
						.insertBefore( $('li:last', an[i])[0] )
						.bind('click', function (e) {
							e.preventDefault();
							oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
							fnDraw( oSettings );
						} );
				}

				// Add / remove disabled classes from the static elements
				if ( oPaging.iPage === 0 ) {
					$('li:first', an[i]).addClass('disabled');
				} else {
					$('li:first', an[i]).removeClass('disabled');
				}

				if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
					$('li:last', an[i]).addClass('disabled');
				} else {
					$('li:last', an[i]).removeClass('disabled');
				}
			}
		}
	}
} );


/*
 * TableTools Bootstrap compatibility
 * Required TableTools 2.1+
 */
if ( $.fn.DataTable.TableTools ) {
	// Set the classes that TableTools uses to something suitable for Bootstrap
	$.extend( true, $.fn.DataTable.TableTools.classes, {
		"container": "DTTT btn-group",
		"buttons": {
			"normal": "btn",
			"disabled": "disabled"
		},
		"collection": {
			"container": "DTTT_dropdown dropdown-menu",
			"buttons": {
				"normal": "",
				"disabled": "disabled"
			}
		},
		"print": {
			"info": "DTTT_print_info modal"
		},
		"select": {
			"row": "active"
		}
	} );

	// Have the collection use a bootstrap compatible dropdown
	$.extend( true, $.fn.DataTable.TableTools.DEFAULTS.oTags, {
		"collection": {
			"container": "ul",
			"button": "li",
			"liner": "a"
		}
	} );
}

$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {

//        for (var i in filter.enabled)   {
//            if (filter.enabled[i])  {
//                var min = filter.values[i].min;
//                var max = filter.values[i].max;
//                if (aData[i] >= min && aData[i] <= max)   {
//                    return true;
//                }
//            }
//        }
        return true;
    }
);

var filter = {
    enabled: {},
    values: {},
//    update: function()  {
//        console.log('UPDATE');return;
//        var that = this;
//
//        var headers = $('#report_table thead tr.filters th');
//
//        $('#report_table thead tr.head th input[type=checkbox]').each(function()    {
//            var columnIndex = $(this).parents('tr').prevAll().length + 1;
//            that.enabled[columnIndex] = this.checked;
//
//            console.log(columnIndex, headers);
//            if (this.checked)   {
//                $(headers[columnIndex-1]).find('div.filter-control').show();
//            }
//            else    {
//                $(headers[columnIndex-1]).find('div.filter-control').hide();
//            }
//
//        })
//    },

    updateData: function()  {
        if (dataTableObject)    {
            dataTableObject.fnDraw();
        }
    },

    getFilter: function(filterType, options) {
        options = options || {};
        var elementIndex = options.index;

        var filterControlWrapper = $('<div class="filter-control"></div>')[0];

        var listeners = options.listeners || {};

        switch(filterType)  {
            case 'select':
                $.fn.dataTableExt.afnFiltering.push(
                    function( oSettings, aData, iDataIndex ) {
                        if (!filter.enabled[elementIndex]) return true;


                        var filterValue = filter.values[elementIndex];

                        console.log(filterValue);

                        if (filterValue === undefined || aData[elementIndex - 1] === filterValue)   {
                            return true
                        }
                        else    {
                            return false;
                        }
                    }
                );


                var filterTemplate = 'js/templates/filters/' + filterType + '.ejs';
                new EJS({url: filterTemplate}).update(filterControlWrapper, options)

                //select first item
                for (var optionValue in options.range)    {
                    filter.values[elementIndex] = options.range[optionValue]
                    break;
                }

                $(filterControlWrapper).find('select').change(function()    {

                    filter.values[elementIndex] = $(this).val();

                    filter.updateData();
                });

                break;

            case 'date-range':

                $.fn.dataTableExt.afnFiltering.push(
                    function( oSettings, aData, iDataIndex ) {
                        if (!filter.enabled[elementIndex]) return true;
                        var dateChunks = aData[elementIndex - 1].split('-');
                        var date = new Date(dateChunks[0], dateChunks[1] - 1, dateChunks[2]);
                        console.log(aData[elementIndex], date);

                        if (date.valueOf() >= filter.values[elementIndex].min.valueOf() && date.valueOf() <= filter.values[elementIndex].max.valueOf())   {
                            return true
                        }
                        else    {
                            return false;
                        }
                    }
                );

                var boundRange = new DateRange();
                boundRange.setMonthRange(-6, 6);

                var defaultRange = new DateRange();
                defaultRange.setMonthRange(0, 1);

                filter.values[elementIndex] = {
                    min: defaultRange.lBound,
                    max: defaultRange.uBound
                };

                $(filterControlWrapper).dateRangeSlider({
                    arrows: false,
                    bounds: {
                        min: boundRange.lBound,
                        max: boundRange.uBound
                    },
                    defaultValues:  filter.values[elementIndex]
                }).bind("valuesChanged", function(e, data){
                        if ($.isFunction(listeners.change)) {
                            listeners.change(e, data);
                        }

                        filter.values[elementIndex] = data.values;

                        filter.updateData();

                    console.log("Values just changed. min: " + data.values.min + " max: " + data.values.max);
                });

                break;

            case 'numbers-range':

                $.fn.dataTableExt.afnFiltering.push(
                    function( oSettings, aData, iDataIndex ) {
                        if (!filter.enabled[elementIndex]) return true;
                        var data = Number(aData[elementIndex - 1]);
                        console.log(data);

                        if (data >= filter.values[elementIndex].min && data <= filter.values[elementIndex].max)   {
                            return true
                        }
                        else    {
                            return false;
                        }
                    }
                );

                var range = new Iterator(options.range).getMinMax();
                console.log('range', range);

                filter.values[elementIndex] = range;

                $(filterControlWrapper).rangeSlider({
                    arrows: false,
                    bounds: range,
                    defaultValues:  range,
                    step: 1
                }).bind("valuesChanged", function(e, data){
                        if ($.isFunction(listeners.change)) {
                            listeners.change(e, data);
                        }

                        filter.values[elementIndex] = data.values;

                        filter.updateData();

                    console.log("Values just changed. min: " + data.values.min + " max: " + data.values.max);
                });

                break;
        }

        return filterControlWrapper;
    },

    showFilter: function(index)  {
        var expr = '#report_table thead tr.filters th:nth-child(' + index + ')';
        console.log(expr)
        var filterTh = $(expr);
        var filterControl = filterTh.find('.filter-control');
        if (filterControl.length)   {
            filterControl.show();
        }
        else    {
            var filterType = filterTh.attr('filter-type');
            console.log('filterType', filterType)
            if (filterType) {

                var range = dataTableObject.fnGetColumnData(index - 1, true, false, false)

                var filterElement = this.getFilter(filterType, {
                    index: index,
                    range: range
                });
                filterTh.append(filterElement);
            }
        }
        this.enabled[index] = true;
        filter.updateData();
    },
    hideFilter: function(index)  {
//        console.log(index, this._getFilterTH(index))
        this._getFilterTH(index).find('.filter-control').hide();
        this.enabled[index] = false;
        filter.updateData();
    },

    _getFilterTH: function(index)   {
        return $('#report_table thead tr.filters th:nth-child(' + index + ')');
    }
};


var calculateTotalSum = function()  {
    var index = $('#report_table thead tr.head th[data-label=amount]').prevAll().length;

    var totalAmount = 0;
    dataTableObject.$('tr', { "filter": "applied" }).each(function()    {
        var amount = $(this).find('td')[index].innerText;
        totalAmount += Number(amount);
    });

    totalAmount = Math.round(totalAmount * 1000) / 1000;

    $('#totalSum').html('$' + totalAmount);
}

/* Table initialisation */
$(document).ready(function() {
	dataTableObject = $('#report_table').dataTable( {
		"sDom": "<'panel-body'<'page-size'l><'table-filter'f>r>t<'panel-body report-pagination'<'pull-left'i><'pull-right'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ records per page"
		},
        "iDisplayLength" : 10,

        'fnDrawCallback': function()    {

            if (dataTableObject && dataTableObject.$)    {
                calculateTotalSum();
            }
        }
	} );

    calculateTotalSum();

    $('.page-size select').addClass('form-control');
    $('.table-filter input').addClass('form-control');
    $('.table-filter input').css('width', '200px');

    $('#report_table thead tr.filters th').each(function()  {


    });

    $('#report_table thead tr.head th input[type=checkbox]').each(function()    {
        $(this).click(function(e)    {
            var columnIndex = $(this).parents('th').prevAll().length + 1;
            console.log('this.checked', this.checked)
            if (this.checked)   {
                filter.showFilter(columnIndex);
            }
            else    {
                filter.hideFilter(columnIndex);
            }

            e.stopPropagation();
        });
    });

    $('#export').click(function()   {
        var exportIds = [];
        dataTableObject.$('tr', { "filter": "applied" }).each(function()    {
            var id = $(this).find('td:first-child').text();
            exportIds.push($.trim(id));
        });

        var languageHeader = $('#report_table thead tr.head th[data-label=language]');
        var languageIndex = languageHeader.prevAll().length + 1;

        var langString = '';
        if (filter.enabled[languageIndex])  {
            var filteredLanguage = filter.values[languageIndex];

            langString = '/' + filteredLanguage;
        }

        if (typeof(exportReportURL) === 'string')   {
            window.location = exportReportURL + exportIds.toString() + langString;
        }
    })
//    filter.update();
//    console.log(filter.enabled)
//
} );